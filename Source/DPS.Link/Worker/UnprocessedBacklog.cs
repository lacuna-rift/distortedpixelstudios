﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using DPS.Link.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using NLog;

namespace DPS.Link.Worker
{
    public class UnprocessedBacklog
    {
        protected static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private readonly ManualResetEventSlim _resetEvent = new ManualResetEventSlim();
        private readonly ConcurrentQueue<(UnprocessedView, UnprocessedViewHeader[], UnprocessedViewCookie[])> _backlog = new ConcurrentQueue<(UnprocessedView, UnprocessedViewHeader[], UnprocessedViewCookie[])>();

        public int Count => _backlog.Count;

        public void Enqueue(LinkRoute route, HttpContext context)
        {
            if (!context.Request.Cookies.TryGetValue("dps_session_id", out var sessionId))
            {
                sessionId = Guid.NewGuid().ToString("N");
            }
            context.Response.Cookies.Append("dps_session_id", sessionId, new CookieOptions
            {
                HttpOnly = false,
                MaxAge = TimeSpan.FromDays(365),
                IsEssential = true,
                Expires = DateTimeOffset.UtcNow.AddYears(1),
                Path = "/",
                Domain = "h"
            });

            var unprocessedView = new UnprocessedView
            {
                Timestamp = DateTime.UtcNow,
                SessionId = sessionId,
                IncommingUrl = context.Request.GetDisplayUrl(),
                OutgoingUrl = route.Destination
            };

            var unprocessedViewHeaders = context.Request.Headers.SelectMany(o => o.Value.Select(v => new UnprocessedViewHeader
            {
                View = unprocessedView,
                Key = o.Key,
                Value = v
            })).ToArray();

            var unprocessedViewCookies = context.Request.Cookies.Select(o => new UnprocessedViewCookie
            {
                View = unprocessedView,
                Key = o.Key,
                Value = o.Value
            }).ToArray();

            _backlog.Enqueue((unprocessedView, unprocessedViewHeaders, unprocessedViewCookies));
            _resetEvent.Set();
        }

        public bool TryDequeue(out (UnprocessedView View, UnprocessedViewHeader[] Headers, UnprocessedViewCookie[] Cookies) unprocessed)
        {
            var result = _backlog.TryDequeue(out unprocessed);
            if (!result)
            {
                _resetEvent.Reset();
            }
            return result;
        }

        public void Wait(CancellationToken cancellationToken)
        {
            _resetEvent.Wait(cancellationToken);
        }
    }
}