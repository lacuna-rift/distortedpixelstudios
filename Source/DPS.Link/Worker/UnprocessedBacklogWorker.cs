using System.Threading;
using System.Threading.Tasks;
using Lacuna.Core;
using Lacuna.Core.Data.Internal.Interfaces;

namespace DPS.Link.Worker
{
    public class UnprocessedBacklogWorker : FhBackgroundService
    {
        private readonly UnprocessedBacklog _backlog;
        private readonly ISessionFactory _sessionFactory;

        public UnprocessedBacklogWorker(UnprocessedBacklog backlog, ISessionFactory sessionFactory)
        {
            _backlog = backlog;
            _sessionFactory = sessionFactory;
        }

        protected override async Task ProcessAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                using (var session = _sessionFactory.Produce())
                {
                    while (_backlog.TryDequeue(out var unprocessed))
                    {
                        await session.InsertAsync(unprocessed.View, stoppingToken);
                        foreach (var header in unprocessed.Headers)
                        {
                            await session.InsertAsync(header, stoppingToken);
                        }
                        foreach (var cookie in unprocessed.Cookies)
                        {
                            await session.InsertAsync(cookie, stoppingToken);
                        }
                    }
                }
                _backlog.Wait(stoppingToken);
            }
        }
    }
}