﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DPS.Link.Models;
using Lacuna.Core.Data.Internal.Interfaces;
using NHibernate.Linq;
using NLog;

namespace DPS.Link.Worker
{
    public class LinkRouteCache
    {
        protected static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly ISessionFactory _sessionFactory;

        private Dictionary<string, LinkRoute> _routes = new Dictionary<string, LinkRoute>();

        public LinkRouteCache(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        public async Task Refresh(CancellationToken cancellationToken)
        {
            using var session = _sessionFactory.Produce();

            var routes = await session.Query<LinkRoute>().ToListAsync(cancellationToken);

            if (!cancellationToken.IsCancellationRequested)
            {
                Log.Info($"<LinkRouteCache> Loaded [{routes.Count}] Links\r\n - " + string.Join("\r\n - ", routes.Select(o => $"[{o.Id}] {o.Signature} -> {o.Destination}")));
                _routes = routes.ToDictionary(o => o.Signature);
            }
        }

        public LinkRoute GetRoute(string signature)
        {
            if (_routes.TryGetValue(signature, out var route))
            {
                return route;
            }
            return null;
        }
    }
}