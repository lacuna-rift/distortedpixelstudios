using System;
using System.Threading;
using System.Threading.Tasks;
using Lacuna.Core;

namespace DPS.Link.Worker
{
    public class LinkRouteCacheWorker : FhBackgroundService
    {
        private readonly LinkRouteCache _routeCache;

        public LinkRouteCacheWorker(LinkRouteCache routeCache)
        {
            _routeCache = routeCache;
        }

        protected override async Task ProcessAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await _routeCache.Refresh(stoppingToken);
                await Task.Delay(TimeSpan.FromMinutes(1), stoppingToken);
            }
        }
    }
}