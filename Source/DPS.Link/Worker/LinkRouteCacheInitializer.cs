﻿using System.Threading;
using Lacuna.Core.Interfaces;

namespace DPS.Link.Worker
{
    public class LinkRouteCacheInitializer : IEnvironmentInitializer
    {
        public int Order => 500000;

        private readonly LinkRouteCache _routeCache;

        public LinkRouteCacheInitializer(LinkRouteCache routeCache)
        {
            _routeCache = routeCache;
        }

        public void Initialize()
        {
            _routeCache.Refresh(CancellationToken.None).Wait();
        }
    }
}
