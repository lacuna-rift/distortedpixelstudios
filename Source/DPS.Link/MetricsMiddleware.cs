﻿using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using NLog;

namespace DPS.Link
{
    public class MetricsMiddleware
    {
        protected static readonly Logger Log = LogManager.GetCurrentClassLogger();

        private readonly RequestDelegate _next;

        public MetricsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var timer = Stopwatch.StartNew();
            
            var uagent = context.Request.Headers.TryGetValue("User-Agent", out var uagents) && uagents.Count > 0 ? uagents[0] : "-";
            var inUrl = context.Request.GetDisplayUrl();
            await _next(context);
            var outUrl = context.Response.Headers.TryGetValue("Location", out var locations) && locations.Count > 0 ? locations[0] : "-";
            var outStatus = (HttpStatusCode)context.Response.StatusCode;

            timer.Stop();
            Log.Info($"[MetricsMiddleware]\r\n" +
                     $"\t{{TIME}} {timer.Elapsed.TotalMilliseconds:#,##0.000} ms\r\n" +
                     $"\t{{IN}} [{context.Request.Method}] {inUrl}\r\n" +
                     $"\t{{OUT}} {outUrl}\r\n" +
                     $"\t{{STATUS}} [{context.Response.StatusCode}] {outStatus}\r\n" +
                     $"\t{{UAGENT}} {uagent}\r\n");

        }
    }
}