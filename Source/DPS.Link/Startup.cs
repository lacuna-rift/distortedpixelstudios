using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using DPS.Link.Models;
using DPS.Link.Worker;
using FluentNHibernate;
using Lacuna.Core.Data;
using Lacuna.Core.Data.Extensions;
using Lacuna.Core.Data.Mapping;
using Lacuna.Core.Extensions;
using Lacuna.Core.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog.Fluent;

namespace DPS.Link
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.RegisterAsMappingAssembly(GetType().Assembly);
            services.RegisterAsMigrationAssembly(GetType().Assembly);
            services.RegisterModule(new DataModule(Program.ApplicationName));

            services.AddSingleton<UnprocessedBacklog>();
            services.AddSingleton<LinkRouteCache>();
            services.AddSingleton<IEnvironmentInitializer, LinkRouteCacheInitializer>();
            services.AddSingleton<UnprocessedBacklogWorker>();
            services.AddSingleton<LinkRouteCacheWorker>();

            ////////////////////////////////////////////////////////////////
            /////>  ENUMS
            ////////////////////////////////////////////////////////////////


            ////////////////////////////////////////////////////////////////
            /////>  TABLES
            ////////////////////////////////////////////////////////////////

            services.RegisterAutoMappedType<LinkRoute>();
            services.RegisterAutoMappedType<UnprocessedView>();
            services.RegisterAutoMappedType<UnprocessedViewHeader>();
            services.RegisterAutoMappedType<UnprocessedViewCookie>();

            ////////////////////////////////////////////////////////////////

            services.AddMvc()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var application = app.ApplicationServices.GetService<IApplication>();
            application.Initialize(Program.ApplicationName, app.ApplicationServices);
            using (var scope = app.ApplicationServices.CreateScope())
            {
                EnsureAllEnumsAreMappedToAppeaseTheDes(scope);
                foreach (var initializer in scope.ServiceProvider.GetServices<IEnvironmentInitializer>().OrderBy(o => o.Order))
                {
                    initializer.Initialize();
                }
            }
            // free up the memory claimed by debug stuff
            AutoLacunaClassMapMigrator.Clear();
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true, true);

            new Thread(() => app.ApplicationServices.GetService<LinkRouteCacheWorker>().StartAsync(CancellationToken.None).Wait(CancellationToken.None)) { IsBackground = true }.Start();
            new Thread(() => app.ApplicationServices.GetService<UnprocessedBacklogWorker>().StartAsync(CancellationToken.None).Wait(CancellationToken.None)) { IsBackground = true }.Start();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<MetricsMiddleware>();
           
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void EnsureAllEnumsAreMappedToAppeaseTheDes(IServiceScope scope)
        {
            if (Debugger.IsAttached) return;

            var hasErrors = false;
            var registeredEnums = new HashSet<Type>();
            foreach (var mappingProvider in scope.ServiceProvider.GetServices<IMappingProvider>())
            {
                var providerType = mappingProvider.GetType();
                var mappedType = providerType.GetGenericArguments()[0];

                if (mappedType.IsEnum) registeredEnums.Add(mappedType);
                else
                {
                    foreach (var propertyInfo in mappedType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
                    {
                        if (propertyInfo.PropertyType.IsEnum && registeredEnums.Add(propertyInfo.PropertyType))
                        {
                            Log.Error($"Type Validation Failed : Enum [{propertyInfo.PropertyType.Name}] is not registered with [RegisterAutoMappedEnum]!");
                            hasErrors = true;
                        }
                    }
                }
            }

            if (hasErrors)
            {
                throw new Exception("Fix the validation errors (Check the console app output)");
            }
        }
    }
}
