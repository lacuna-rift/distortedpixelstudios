using System.Diagnostics.CodeAnalysis;
using Lacuna.Core.Data.Migrator;

namespace DPS.Link.Models.Migrations
{
    [LacunaMigration(2020, 05, 04, 00, 01)]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    internal class M20200504_Add_UnprocessedView_SessionId : LacunaMigration
    {
        public override void Up()
        {
            Alter.Table("UnprocessedView")
                .AddColumn("SessionId").AsString(100).NotNullable();
        }
    }
}
