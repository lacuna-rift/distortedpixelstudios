﻿using Lacuna.Core.Data.Mapping;

namespace DPS.Link.Models
{
    public class UnprocessedViewCookie
    {
        public virtual long Id { get; set; }

        [Reference]
        public virtual UnprocessedView View { get; set; }

        [StringLength(100)]
        public virtual string Key { get; set; }

        [StringLength(8 * 1024)] // http spec max 8kb
        public virtual string Value { get; set; }
    }
}