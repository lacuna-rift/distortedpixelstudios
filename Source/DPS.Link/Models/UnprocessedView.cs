﻿using System;
using Lacuna.Core.Data.Mapping;

namespace DPS.Link.Models
{
    public class UnprocessedView
    {
        public virtual long Id { get; set; }

        public virtual DateTime Timestamp { get; set; }

        [StringLength(100)]
        public virtual string SessionId { get; set; }

        [StringLength(1024)]
        public virtual string IncommingUrl { get; set; }

        [StringLength(1024)]
        public virtual string OutgoingUrl { get; set; }
    }
}