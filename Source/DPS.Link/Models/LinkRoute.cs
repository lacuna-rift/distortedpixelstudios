﻿using System;
using Lacuna.Core.Data.Mapping;

namespace DPS.Link.Models
{
    public class LinkRoute
    {
        public virtual short Id { get; set; }

        public virtual DateTime Creation { get; set; }

        [StringLength(255)]
        public virtual string Signature { get; set; }

        [StringLength(1024)]
        public virtual string Destination { get; set; }

        [StringLength(1000)]
        public virtual string Notes { get; set; }
    }
}
