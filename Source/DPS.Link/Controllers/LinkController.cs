﻿using System;
using System.Net;
using System.Threading.Tasks;
using DPS.Link.Worker;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace DPS.Link.Controllers
{
    [Controller]
    public class LinkController : ControllerBase
    {
        protected static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private readonly LinkRouteCache _routeCache;
        private readonly UnprocessedBacklog _unprocessedBacklog;

        public LinkController(LinkRouteCache routeCache, UnprocessedBacklog unprocessedBacklog)
        {
            _routeCache = routeCache;
            _unprocessedBacklog = unprocessedBacklog;
        }

        [HttpGet, Route("")]
        public IActionResult Default()
        {
            return Redirect("https://www.youtube.com/channel/UCO8iVbK5YAT_Gmd41WwzJ2w?sub_confirmation=1");
        }

        [HttpGet, Route("404")]
        public async Task FourOhFour()
        {
            Response.Clear();
            Response.StatusCode = (int)HttpStatusCode.NotFound;
            Response.ContentType = "text/html";
            await Response.SendFileAsync("404.html");
        }

        [HttpGet, Route("favicon.ico")]
        public async Task FavIcon()
        {
            Response.Clear();
            Response.StatusCode = (int)HttpStatusCode.OK;
            Response.ContentType = "image/x-icon";
            await Response.SendFileAsync("favicon.ico");
        }

        [HttpGet, Route("{signature}")]
        public IActionResult Go(string signature)
        {
            var route = _routeCache.GetRoute(signature);

            if (route == null)
            {
                return Redirect("/404");
            }

            try
            {
                _unprocessedBacklog.Enqueue(route, ControllerContext.HttpContext);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }

            return Redirect(route.Destination);
        }
    }
}
