dotnet publish -c Release

gcloud compute ssh dps-tips --command="sudo rm -rf /mnt/stateful_partition/lacuna-data/app/*"
gcloud compute scp --recurse  ./bin/Release/netcoreapp3.1/publish/ dps-tips:/mnt/stateful_partition/lacuna-data/app
gcloud compute ssh dps-tips --command="sh /mnt/stateful_partition/lacuna-data/deploy.sh"