using Microsoft.Extensions.DependencyInjection;

namespace DPS.Server.Auth
{
    public static class FhUserAuthenticationExtensions
    {
        public static void AddFhAuthenticationAndAuthorization(this IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.AddScheme<FhUserAuthenticationHandler>("FhAuthentication", "Fatality Hosting Authentication");
                options.DefaultScheme = "FhAuthentication";
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("FhAuthenticatedUser",
                    policy => policy.AddRequirements(new FhUserRequirement()));

                options.DefaultPolicy = options.GetPolicy("FhAuthenticatedUser");
            });
        }
    }
}