using System;
using DPS.Server.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace DPS.Server.Auth
{
    public abstract class AuthenticatedController : ControllerBase
    {
        public new AccountDto User
        {
            get
            {
                if (!(base.User.Identity is AuthenticatableIdentity identity))
                {
                    return null;
                }

                if (!identity.IsAuthenticated)
                {
                    return null;
                }

                return identity.Account;
            }
        }

        public Uri Referer => Request.Headers.ContainsKey("Referer") ? new Uri(Request.Headers["Referer"].ToString()) : null;
    }
}