using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace DPS.Server.Auth
{
    public class FhUserRequirement : AuthorizationHandler<FhUserRequirement>, IAuthorizationRequirement
    {
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, FhUserRequirement requirement)
        {
            if (context.User?.Identity is AuthenticatableIdentity fhIdentity)
            {
                await fhIdentity.Authenticate();
                if (fhIdentity.IsAuthenticated)
                {
                    context.Succeed(requirement);
                    return;
                }
            }
            context.Fail();
        }
    }
}