using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DPS.Server.Auth
{
    public class RequiredAuthenticationAttribute : ActionFilterAttribute
    {
        private static readonly string[] EmptyRoles = { };
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            // Check if the token is available

            if (!(context.HttpContext.User.Identity is AuthenticatableIdentity identity))
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            // Load Account
            await identity.Authenticate();

            // Check for existence (and implicitly state)
            if (identity?.IsAuthenticated != true)
            {
                context.Result = new UnauthorizedResult();
                return;
            }

            await next();
        }
    }
}