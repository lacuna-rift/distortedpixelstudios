using System;
using System.Collections.Concurrent;
using System.Security.Principal;
using System.Threading.Tasks;
using DPS.Server.DataLayer.Repositories;
using DPS.Server.Dtos;
using DPS.Server.Mappers;

namespace DPS.Server.Auth
{
    public class AuthenticatableIdentity : IIdentity
    {
        private readonly AccountRepository _accountRepository;
        private readonly int _accountId;
        private bool _authenticationAttempted;

        public AccountDto Account { get; private set; }
        public string AuthenticationType => "Token";
        public bool IsAuthenticated => Account?.Id > 0;
        public string Name { get; private set; }

        public AuthenticatableIdentity(int accountId, AccountRepository accountRepository)
        {
            _accountId = accountId;
            _accountRepository = accountRepository;
            Name = $"Unauthenticated user #{accountId}";
        }


        private static readonly TimeSpan CacheValidTimespan = TimeSpan.FromSeconds(5);
        private static readonly ConcurrentDictionary<int, (AccountDto account, DateTime expirey)> Cache = new ConcurrentDictionary<int, (AccountDto account, DateTime expirey)>();

        public async Task Authenticate()
        {
            if (!_authenticationAttempted)
            {
                if (Cache.TryGetValue(_accountId, out var record) && record.expirey > DateTime.UtcNow)
                {
                    Account = record.account;
                }
                else
                {
                    Account =  Mapper.MapAccount(await _accountRepository.Get(_accountId));
                    Cache[_accountId] = (Account, DateTime.UtcNow + CacheValidTimespan);
                }
                _authenticationAttempted = true;

                if (IsAuthenticated)
                {
                    Name = $"{Account?.DisplayName}";
                }
                else
                {
                    Name = $"Unauthenticated user #{_accountId}";
                }
            }
        }
    }
}