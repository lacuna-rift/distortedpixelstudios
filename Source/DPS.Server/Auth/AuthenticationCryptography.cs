using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DPS.Server.Auth
{
    public class AuthenticationCryptography
    {
        private static readonly byte[] EncryptionSalt = new Guid("99cf22d6-3356-4d33-92f5-2451ee030c5e").ToByteArray();
        private static readonly byte[] HashingSalt = new Guid("3f938452-f411-4e49-bfc8-060f3cf14970").ToByteArray();

        private DateTime CurrentEncryptionPeriod
        {
            get
            {
                var now = DateTime.Now;
                return new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            }
        }

        private DateTime PreviousEncryptionPeriod(DateTime currentPeriod)
        {
            return currentPeriod.AddDays(-1);
        }

        private static byte[] PeriodToKey(DateTime input)
        {
            var output = new byte[sizeof(int) * 4];
            BitConverter.GetBytes(input.Year).CopyTo(output, sizeof(int) * 0);
            BitConverter.GetBytes(input.Month).CopyTo(output, sizeof(int) * 1);
            BitConverter.GetBytes(input.Day).CopyTo(output, sizeof(int) * 2);
            //BitConverter.GetBytes(input.Hour).CopyTo(output, sizeof(int) * 3);
            return output;
        }

        public class EncryptResult
        {
            public string Data { get; set; }

            public DateTime RefreshAvailableFrom { get; set; }
            public DateTime RefreshAvailableUntil { get; set; }

            public bool Failed { get; set; }

            public EncryptResult(string data, DateTime currentPeriod)
            {
                Data = data;
                RefreshAvailableFrom = currentPeriod.AddDays(1);
                RefreshAvailableUntil = RefreshAvailableFrom.AddDays(1);
            }
        }

        public class DecryptResult<T>
        {
            public T Data { get; set; }
            public bool Failed { get; set; }

            // JsonConverter needs this
            public DecryptResult() { }
        }

        public async Task<EncryptResult> Encrypt<T>(T rawValue)
        {
            var currentPeriod = CurrentEncryptionPeriod;
            var currentKey = PeriodToKey(currentPeriod);

            var objJson = JsonConvert.SerializeObject(rawValue);

            using (var aesAlg = Aes.Create())
            using (var encryptor = aesAlg.CreateEncryptor(currentKey, GetIV(currentKey)))
            using (var msEncrypt = new MemoryStream())
            {
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                using (var swEncrypt = new StreamWriter(csEncrypt))
                {
                    await swEncrypt.WriteAsync(objJson);
                }

                var encrypted = msEncrypt.ToArray();
                var b64 = Convert.ToBase64String(encrypted);
                var output = new EncryptResult(b64, currentPeriod);
                return output;
            }
        }

        public bool TryDecrypt<T>(string cipherText, out T data)
        {
            var result = Decrypt<T>(cipherText).Result;
            if (!result.Failed)
            {
                data = result.Data;
                return true;
            }
            data = default;
            return false;
        }

        public async Task<DecryptResult<T>> Decrypt<T>(string cipherText)
        {
            var currentPeriod = CurrentEncryptionPeriod;
            var currentKey = PeriodToKey(currentPeriod);

            var result = await Decrypt<T>(cipherText, currentKey);
            if (!result.Failed)
                return result;

            var previousPeriod = PreviousEncryptionPeriod(currentPeriod);
            var previousKey = PeriodToKey(previousPeriod);
            return await Decrypt<T>(cipherText, previousKey);
        }

        private async Task<DecryptResult<T>> Decrypt<T>(string cipherText, byte[] key)
        {
            try
            {
                var cipherBytes = Convert.FromBase64String(cipherText);

                using (var aesAlg = Aes.Create())
                using (var decryptor = aesAlg.CreateDecryptor(key, GetIV(key)))
                using (var msDecrypt = new MemoryStream(cipherBytes))
                using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                using (var srDecrypt = new StreamReader(csDecrypt))
                {
                    var resultRaw = await srDecrypt.ReadToEndAsync();
                    var result = JsonConvert.DeserializeObject<T>(resultRaw);
                    return new DecryptResult<T>
                    {
                        Data = result,
                        Failed = false
                    };
                }
            }
            catch
            {
                return new DecryptResult<T>
                {
                    Failed = true
                };
            }
        }

        private byte[] GetIV(byte[] key)
        {
            var pdb = new Rfc2898DeriveBytes(BitConverter.ToString(key), EncryptionSalt);
            return pdb.GetBytes(key.Length);
        }

        public string Hash(string content)
        {
            using (var hasher = new HMACSHA512(HashingSalt))
            {
                return Convert.ToBase64String(hasher.ComputeHash(Encoding.UTF8.GetBytes(content ?? "")));
            }
        }
    }
}