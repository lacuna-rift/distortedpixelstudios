using System;
using System.Security.Principal;
using System.Threading.Tasks;
using DPS.Server.DataLayer.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace DPS.Server.Auth
{
    public class FhUserAuthenticationHandler : IAuthenticationHandler
    {
        private readonly AuthenticationCryptography _cryptography;
        private readonly AccountRepository _accountRepository;
        private AuthenticationScheme _scheme;
        private HttpContext _context;

        public FhUserAuthenticationHandler(AuthenticationCryptography cryptography, AccountRepository accountRepository)
        {
            _cryptography = cryptography;
            _accountRepository = accountRepository;
        }

        public async Task<AuthenticateResult> AuthenticateAsync()
        {
            // Check if the token is available

            if (!_context.Request.Query.TryGetValue("auth-token", out var tokens)) // Manual
            {
                if (!_context.Request.Headers.TryGetValue("x-auth-token", out tokens)) // RestApi
                {
                    _context.Request.Query.TryGetValue("access_token", out tokens); // SignalR
                }
            }

            // Check if the token is available
            if (tokens.Count != 0)
            {
                // Check if the token format & expiration is valid
                if (tokens.Count != 1 || !_cryptography.TryDecrypt(tokens[0], out int requestId))
                {
                    return AuthenticateResult.Fail("Malformed authentication token");
                }

                var identity = new AuthenticatableIdentity(requestId, _accountRepository);
                var principal = new GenericPrincipal(identity, new string[0]);
                var ticket = new AuthenticationTicket(principal, _scheme.Name);
                return AuthenticateResult.Success(ticket);
            }

            return AuthenticateResult.NoResult();
        }

        public Task ChallengeAsync(AuthenticationProperties properties)
        {
            throw new NotImplementedException();
        }

        public Task ForbidAsync(AuthenticationProperties properties)
        {
            throw new NotImplementedException();
        }


        public async Task InitializeAsync(AuthenticationScheme scheme, HttpContext context)
        {
            _scheme = scheme;
            _context = context;
        }
    }
}