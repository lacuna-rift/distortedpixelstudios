﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using DPS.Server.Config;
using DPS.Server.DataLayer.Models;
using DPS.Server.Logic.Clients;
using DPS.Server.Workers.Abstract;
using Google.Apis.Auth.OAuth2;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using Lacuna.Core.Data.Extensions;
using Lacuna.Core.Data.Persistance.Interfaces;
using Newtonsoft.Json;

namespace DPS.Server.Workers
{
    public class YoutubeStatUpdateWorker : Worker
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly YoutubeStatUpdateConfig _config;

        public YoutubeStatUpdateWorker(IUnitOfWork unitOfWork, YoutubeStatUpdateConfig config)
        {
            _unitOfWork = unitOfWork;
            _config = config;
        }

        protected override async Task<TimeSpan> DoWork()
        {
            if (Debugger.IsAttached)
                return TimeSpan.FromSeconds(1);
            var credential = _unitOfWork.Query<YoutubeCredential>().FirstOrDefault(o => o.LockedUntil < DateTime.UtcNow);
            if (credential == null)
                return TimeSpan.FromSeconds(10);

            var rawStats = new Queue<YoutubeRawStat>();
            var gCredential = GoogleCredential.FromJson(JsonConvert.SerializeObject(credential))
                .CreateScoped(YouTubeService.Scope.Youtube, YouTubeService.Scope.YoutubeUpload, YouTubeService.Scope.YoutubeReadonly, YouTubeService.Scope.Youtubepartner, YouTubeService.Scope.YoutubeForceSsl);

            var api = new DpsYoutubeApi(gCredential);

            var costTotalAfter = api.ApiCallCosts.Sum(o => o.Item2);
            var costTotal = costTotalAfter;
            foreach (var (channelId, owned) in _config.ChannelsToScan)
            {
                var costBefore = api.ApiCallCosts.Sum(o => o.Item2);
                var channel = await api.GetChannelDetails(channelId);
                var vids = (await api.GetVideoDetails(channel)).ToDictionary(o => o.ResourceId.VideoId);
                var stats = await api.GetVideoStatistics(vids.Keys.ToArray());
                var date = DateTime.UtcNow;
                var data = stats.Select(o => new YoutubeRawStat
                {
                    Timestamp = date,

                    ChannelId = vids[o.Key].ChannelId,
                    ChannelSubs = (int)channel.Statistics.SubscriberCount.Value,
                    ChannelName = vids[o.Key].ChannelTitle,

                    VideoId = o.Key,
                    VideoDate = vids[o.Key].PublishedAt.Value,
                    VideoName = vids[o.Key].Title,

                    CommentCount = (int)o.Value.CommentCount,
                    DislikeCount = (int)o.Value.DislikeCount,
                    FavoriteCount = (int)o.Value.FavoriteCount,
                    LikeCount = (int)o.Value.LikeCount,
                    ViewCount = (int)o.Value.ViewCount,
                }).ToArray();

                foreach (var youtubeRawStat in data)
                {
                    rawStats.Enqueue(youtubeRawStat);
                }

                if (owned)
                {
                    await DoWorkForOurChannel(api, channel, vids, stats);
                }
                var costAfter = api.ApiCallCosts.Sum(o => o.Item2);
                Console.WriteLine($"Processed {vids.Count} vids for channel [{channel.Id}] using [{costAfter - costBefore}] api points");
            }
            costTotalAfter = api.ApiCallCosts.Sum(o => o.Item2);

            Console.WriteLine("Total Points : " + costTotalAfter);
            Console.WriteLine("Thread Done : " + DateTime.Now);
            var sleepTime = TimeSpan.FromDays(1)
                            / (10000 - 500) // Total Api points available - safety
                            * (costTotalAfter - costTotal); // points spent
            var sleepUntil = DateTime.UtcNow + sleepTime;

            Console.WriteLine("Sleep Time : " + sleepTime);
            Console.WriteLine("Total Points : " + costTotalAfter);
            Console.WriteLine("Total Stats : " + rawStats.Count);

            await _unitOfWork.Query<YoutubeCredential>()
                .Where(o => o.Id == credential.Id)
                .UpdateSet(o => o.LockedUntil, sleepUntil)
                .ExecuteUpdateAsync();
            _unitOfWork.Commit();

            while (rawStats.TryDequeue(out var rawStat))
            {
                await _unitOfWork.InsertAsync(rawStat); // no async please
            }
            _unitOfWork.Commit();

            return TimeSpan.FromSeconds(10);
        }

        private async Task DoWorkForOurChannel(DpsYoutubeApi api, Channel channel, Dictionary<string, PlaylistItemSnippet> vids, Dictionary<string, VideoStatistics> stats)
        {
            var playlists = await api.GetPlaylists(channel.Id);
            var playlistVids = new (YoutubePlaylist Playlist, HashSet<string> VideoIds)[playlists.Length];
            for (var i = 0; i < playlists.Length; i++)
            {
                var playlist = playlists[i];

                var storedPlaylist = _unitOfWork.Query<YoutubePlaylist>().SingleOrDefault(o => o.RemoteId == playlist.RemoteId);
                if (storedPlaylist == null)
                {
                    storedPlaylist = playlist;
                    await _unitOfWork.InsertAsync(storedPlaylist);
                }
                playlistVids[i] = (storedPlaylist, new HashSet<string>(await api.GetPlaylistVideoIds(storedPlaylist.RemoteId)));
                playlists[i] = storedPlaylist;
            }
            _unitOfWork.Commit();


            foreach (var kvp in vids)
            {
                YoutubePlaylist storedPlaylist = null;
                foreach (var (playlist, playlistVidIds) in playlistVids)
                {
                    if (playlistVidIds.Contains(kvp.Key))
                    {
                        storedPlaylist = playlist;
                        break;
                    }
                }
                if (storedPlaylist == null) continue;

                var storedVid = _unitOfWork.Query<YoutubeVideo>().SingleOrDefault(o => o.RemoteId == kvp.Key);
                if (storedVid == null)
                {
                    storedVid = new YoutubeVideo
                    {
                        RemoteId = kvp.Key,
                        Name = "",
                        Description = "",
                        DisplayDescription = "",
                        PublishDate = kvp.Value.PublishedAt.Value,
                        Playlist = storedPlaylist,
                        Public = true,
                        Thumbnail = "",

                        Likes = 0,
                        Comments = 0,
                        Views = 0,

                        Downloadables = 0,
                        DisplayOrder = -kvp.Value.PublishedAt.Value.Ticks
                    };
                    await _unitOfWork.InsertAsync(storedVid);
                }

                storedVid.Name = kvp.Value.Title;
                if (storedVid.Description.Equals(storedVid.DisplayDescription))
                    storedVid.DisplayDescription = kvp.Value.Description;
                storedVid.Description = kvp.Value.Description;
                storedVid.Thumbnail = kvp.Value.Thumbnails.Maxres.Url;
                storedVid.Playlist = storedPlaylist;
                await _unitOfWork.Query<YoutubeVideo>().Where(o => o.Id == storedVid.Id)
                    .UpdateSet(o => o.Name, storedVid.Name)
                    .UpdateSet(o => o.Description, storedVid.Description)
                    .UpdateSet(o => o.DisplayDescription, storedVid.DisplayDescription)
                    .UpdateSet(o => o.Thumbnail, storedVid.Thumbnail)
                    .UpdateSet(o => o.Playlist, storedPlaylist)
                    .ExecuteUpdateAsync();


                var currentStat = stats[kvp.Key];
                storedVid.Likes = (int)currentStat.LikeCount.Value;
                storedVid.Comments = (int)currentStat.CommentCount.Value;
                storedVid.Views = (int)currentStat.ViewCount.Value;
                storedVid.Downloadables = _unitOfWork.Query<YoutubeVideoDownloadables>().Count(o => o.Video.Id == storedVid.Id);
                await _unitOfWork.Query<YoutubeVideo>().Where(o => o.Id == storedVid.Id)
                    .UpdateSet(o => o.Likes, storedVid.Likes)
                    .UpdateSet(o => o.Comments, storedVid.Comments)
                    .UpdateSet(o => o.Views, storedVid.Views)
                    .UpdateSet(o => o.Downloadables, storedVid.Downloadables)
                    .ExecuteUpdateAsync();

                var comments = await api.GetVideoComments(kvp.Key);
                foreach (var comment in comments.OrderBy(o => o.PublishDate))
                {
                    comment.Video = storedVid;

                    var storedComment = _unitOfWork.Query<YoutubeVideoComment>().SingleOrDefault(o => o.RemoteId == comment.RemoteId);
                    if (storedComment == null)
                    {
                        storedComment = comment;
                        storedComment.DisplayOrder = -comment.PublishDate.Ticks;
                        await _unitOfWork.InsertAsync(storedComment);
                    }

                    storedComment.Text = comment.Text;
                    storedComment.Likes = comment.Likes;
                    await _unitOfWork.Query<YoutubeVideoComment>().Where(o => o.Id == storedComment.Id)
                        .UpdateSet(o => o.Likes, storedComment.Likes)
                        .UpdateSet(o => o.Text, storedComment.Text)
                        .ExecuteUpdateAsync();
                }
                _unitOfWork.Commit();
            }
        }
    }
}