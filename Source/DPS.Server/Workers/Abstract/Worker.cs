﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

namespace DPS.Server.Workers.Abstract
{
    public abstract class Worker
    {
        private readonly BackgroundWorker _worker;

        protected virtual bool RunOnceOnly => false;

        protected Worker()
        {
            _worker = new BackgroundWorker { WorkerSupportsCancellation = true };
            _worker.DoWork += _worker_DoWork;
        }

        public void Start()
        {
            Setup();
            _worker.RunWorkerAsync();
        }

        private void _worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                try
                {
                    var sleepTime = DoWork().Result;
                    if (RunOnceOnly)
                        break;
                    if (sleepTime > TimeSpan.Zero)
                        Thread.Sleep(sleepTime);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    Thread.Sleep(TimeSpan.FromSeconds(15));
                }
            }
        }

        protected virtual void Setup() { }
        protected abstract Task<TimeSpan> DoWork();
    }
}
