﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DPS.Server.Config;
using DPS.Server.DataLayer.Models;
using DPS.Server.DataLayer.Models.Enums;
using DPS.Server.Workers.Abstract;
using Lacuna.Core.Data.Extensions;
using Lacuna.Core.Data.Persistance.Interfaces;
using Newtonsoft.Json;
using NHibernate.Linq;

namespace DPS.Server.Workers
{
    public class DownloadablesWorker : Worker
    {
        private readonly YoutubeVideoDownloadablesConfig _config;
        private readonly IUnitOfWork _unitOfWork;

        public DownloadablesWorker(YoutubeVideoDownloadablesConfig config, IUnitOfWork unitOfWork)
        {
            _config = config;
            _unitOfWork = unitOfWork;
        }

        protected override async Task<TimeSpan> DoWork()
        {
            var knownDownloadablesList = await _unitOfWork.Query<YoutubeVideoDownloadables>()
                .Fetch(o => o.Video)
                .ToListAsync();

            var videos = _unitOfWork.Query<YoutubeVideo>()
                .ToDictionary(o => o.RemoteId);

            var knownDownloadablesDic = knownDownloadablesList.GroupBy(o => o.Video.RemoteId).ToDictionary(o => o.Key, o => o.ToDictionary(d => d.Payload));

            foreach (var indexFile in Directory.GetFiles(_config.DataDirectory, "index.json", SearchOption.AllDirectories))
            {
                var indexJson = await File.ReadAllTextAsync(indexFile);
                var index = JsonConvert.DeserializeObject<DownloadableIndex>(indexJson);

                if (!videos.TryGetValue(index.VideoRemoteId, out var video)) continue;
                var knownDownloadables = knownDownloadablesDic.GetValueOrDefault(index.VideoRemoteId) ?? new Dictionary<string, YoutubeVideoDownloadables>();

                for (var i = 0; i < index.Downloadables.Length; i++)
                {
                    var downloadable = index.Downloadables[i];
                    var fullFilePath = Path.Combine(Directory.GetParent(indexFile).FullName, downloadable.Payload);
                    if (!knownDownloadables.Remove(fullFilePath, out var youtubeVideoDownloadable))
                    {
                        youtubeVideoDownloadable = new YoutubeVideoDownloadables
                        {
                            Video = video,
                            Description = "",
                            Name = "",
                            DisplayOrder = i,
                            Type = YoutubeVideoDownloadableTypes.UnityPackage,
                            Payload = fullFilePath,
                            Downloads = 0,
                            Visible = false,
                            Handle = Guid.NewGuid().ToString("N")
                        };
                        await _unitOfWork.InsertAsync(youtubeVideoDownloadable);
                    }

                    youtubeVideoDownloadable.Description = downloadable.Description;
                    youtubeVideoDownloadable.Name = downloadable.Name;
                    youtubeVideoDownloadable.DisplayOrder = i;
                    youtubeVideoDownloadable.Visible = true;
                    await _unitOfWork.Query<YoutubeVideoDownloadables>()
                           .Where(o => o.Id == youtubeVideoDownloadable.Id)
                           .UpdateSet(o => o.Name, youtubeVideoDownloadable.Name)
                           .UpdateSet(o => o.Description, youtubeVideoDownloadable.Description)
                           .UpdateSet(o => o.DisplayOrder, youtubeVideoDownloadable.DisplayOrder)
                           .UpdateSet(o => o.Visible, youtubeVideoDownloadable.Visible)
                           .ExecuteUpdateAsync();
                }

                foreach (var youtubeVideoDownloadable in knownDownloadables.Values)
                {
                    await _unitOfWork.Query<YoutubeVideoDownloadables>()
                        .Where(o => o.Id == youtubeVideoDownloadable.Id)
                        .UpdateSet(o => o.Visible, false)
                        .ExecuteUpdateAsync();
                }
            }

            _unitOfWork.Commit();
            return TimeSpan.FromMinutes(1);
        }

        private class DownloadableIndex
        {
            public string VideoRemoteId { get; set; }
            public Downloadable[] Downloadables { get; set; }
        }

        private class Downloadable
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public string Type { get; set; }
            public string Payload { get; set; }
        }
    }
}
