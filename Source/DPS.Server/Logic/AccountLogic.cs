using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DPS.Server.Auth;
using DPS.Server.DataLayer.Models;
using DPS.Server.DataLayer.Models.Enums;
using DPS.Server.DataLayer.Repositories;

namespace DPS.Server.Logic
{
    public class AccountLogic
    {
        private readonly AccountRepository _accountRepository;
        private readonly AuthenticationCryptography _authenticationCryptography;

        public AccountLogic(
            AccountRepository accountRepository,
            AuthenticationCryptography authenticationCryptography)
        {
            _accountRepository = accountRepository;
            _authenticationCryptography = authenticationCryptography;
        }

        public async Task<Account> GetActiveById(int accountId)
        {
            var account = await _accountRepository.Get(accountId);
            if (account?.State > AccountState.Active) return null;
            return account;
        }

        public async Task<Account> GetActiveByCredentials(string email, string password)
        {
            var passwordHash = _authenticationCryptography.Hash(password);

            var account = await _accountRepository.Get(email, passwordHash);
            if (account?.State > AccountState.Active) return null;
            return account;
        }

        public async Task<Account> UpdateLastSignIn(int accountId)
        {
            var account = await _accountRepository.Get(accountId);
            return await UpdateLastSignIn(account);
        }

        private async Task<Account> UpdateLastSignIn(Account account)
        {
            account.LastSignin = DateTime.UtcNow;
            await _accountRepository.UpdateLastSignIn(account.Id, account.LastSignin);
            return account;
        }

        public async Task<Account> Create(string email, string password, int? referrerId = null)
        {
            var accountIdForEmail = await _accountRepository.GetAccountIdForEmail(email);

            if (accountIdForEmail.HasValue) return null; // Email address already in use

            var account = new Account();

            account.DisplayName = null;
            account.Email = email;
            account.PasswordHash = _authenticationCryptography.Hash(password);
            account.Creation = DateTime.UtcNow;;
            account.LastSignin = account.Creation;

            await _accountRepository.Create(account);

            return account;
        }

        public async Task<bool> UpdatePassword(Account account, string unhashedCurrentPassword,
            string unhashedNewPassword)
        {
            if (string.IsNullOrWhiteSpace(unhashedNewPassword)) return false; // No password
            if (!new Regex("").IsMatch(unhashedNewPassword)) return false; // Invalid Password format
            if (account.PasswordHash != _authenticationCryptography.Hash(unhashedCurrentPassword)) return false;


            account.PasswordHash = _authenticationCryptography.Hash(unhashedNewPassword);
            await _accountRepository.UpdatePassword(account.Id, account.PasswordHash);

            return true;
        }

        public async Task<bool> RecoverAccountPassword(string email)
        {
            //var sendMailDto = new SendMailDto
            //{
            //    CustomSubject = "Fatality Hosting Password Recovery",
            //    SendToAddress = email,
            //    BodyTemplateType = MailBodyTemplateType.RecoverPassword
            //};

            //await _sendMailPublisher.PublishAsync(new SendMailEvent
            //{
            //    Action = EventAction.Created,
            //    Result = sendMailDto,
            //    Timestamp = DateTime.UtcNow
            //});

            return true;
        }
    }
}