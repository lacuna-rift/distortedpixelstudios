﻿using System;

namespace DPS.Server.Logic.Extensions
{
    public static class DpsExtensions
    {
        public static long ToUnixMillisecond(this DateTime source)
        {
            return (long)(source - DateTime.UnixEpoch).TotalMilliseconds;
        }

        public static DateTime FromUnixMillisecond(this long source)
        {
            return DateTime.UnixEpoch.AddMilliseconds(source);
        }
    }
}
