﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DPS.Server.DataLayer.Models;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using Lacuna.Core.Extensions;

namespace DPS.Server.Logic.Clients
{
    public class DpsYoutubeApi : IDisposable
    {
        private readonly YouTubeService _service;
        public ConcurrentQueue<(DateTime, int)> ApiCallCosts = new ConcurrentQueue<(DateTime, int)>();

        public DpsYoutubeApi(GoogleCredential credential)
        {
            _service = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = typeof(Program).ToString()
            });
        }

        public async Task<Channel> GetChannelDetails(string channelId)
        {
            //https://developers.google.com/youtube/v3/docs/channels/list#part
            var request = _service.Channels.List("contentDetails,statistics");
            request.Id = channelId;
            //channelsListRequest.Mine = true;

            // Retrieve the contentDetails part of the channel resource for the authenticated user's channel.
            var response = await request.ExecuteAsync();
            ApiCallCosts.Enqueue((DateTime.UtcNow, 1 + 2 + 2));

            return response.Items[0];
        }

        public Task<List<PlaylistItemSnippet>> GetVideoDetails(Channel channel) => GetVideoDetails(channel.ContentDetails.RelatedPlaylists.Uploads);
        public async Task<List<PlaylistItemSnippet>> GetVideoDetails(string playlistId)
        {
            var items = new List<PlaylistItemSnippet>();

            var nextPageToken = "";
            while (nextPageToken != null)
            {
                var request = _service.PlaylistItems.List("snippet");
                request.PlaylistId = playlistId;
                request.MaxResults = 50;
                request.PageToken = nextPageToken;

                // Retrieve the list of videos uploaded to the authenticated user's channel.
                var response = await request.ExecuteAsync();
                ApiCallCosts.Enqueue((DateTime.UtcNow, 1 + 2));

                items.AddRange(response.Items.Select(o => o.Snippet));

                nextPageToken = response.NextPageToken;
            }
            return items;
        }

        public Task<Dictionary<string, VideoStatistics>> GetVideoStatistics(IList<PlaylistItemSnippet> items) => GetVideoStatistics(items.Select(o => o.ResourceId.VideoId).ToArray());
        public async Task<Dictionary<string, VideoStatistics>> GetVideoStatistics(params string[] videoIds)
        {
            var results = new Dictionary<string, VideoStatistics>();
            foreach (var batch in videoIds.Distinct().Batch(50))
            {
                var request = _service.Videos.List("statistics");
                request.Id = string.Join(",", batch);

                var response = await request.ExecuteAsync();
                ApiCallCosts.Enqueue((DateTime.UtcNow, 1 + 2));

                foreach (var (key, value) in response.Items.ToDictionary(o => o.Id, o => o.Statistics))
                {
                    results[key] = value;
                }
            }
            return results;
        }

        public async Task<YoutubeVideoComment[]> GetVideoComments(string videoId)
        {
            var request = _service.CommentThreads.List("snippet,replies");
            request.VideoId = videoId;
            request.MaxResults = 100;
            request.TextFormat = CommentThreadsResource.ListRequest.TextFormatEnum.Html;

            var response = await request.ExecuteAsync();
            ApiCallCosts.Enqueue((DateTime.UtcNow, 1 + 2 + 2));

            var topLevel = response
                .Items
                .Select(o => (ytc: new YoutubeVideoComment
                {
                    Author = o.Snippet.TopLevelComment.Snippet.AuthorDisplayName,
                    Thumbnail = o.Snippet.TopLevelComment.Snippet.AuthorProfileImageUrl,
                    Likes = (int)o.Snippet.TopLevelComment.Snippet.LikeCount.Value,
                    PublishDate = o.Snippet.TopLevelComment.Snippet.PublishedAt.Value,
                    Text = o.Snippet.TopLevelComment.Snippet.TextDisplay,
                    RemoteId = o.Id
                }, o))
                .ToArray();

            var subLevels = topLevel
                .Where(item => item.o.Replies != null)
                .SelectMany(item => item.o.Replies.Comments.Select(r => (item.ytc, r)))
                .Select(item => (ytc: new YoutubeVideoComment
                {
                    Author = item.r.Snippet.AuthorDisplayName,
                    Thumbnail = item.r.Snippet.AuthorProfileImageUrl,
                    ReplyTo = item.ytc,
                    Likes = (int)item.r.Snippet.LikeCount.Value,
                    PublishDate = item.r.Snippet.PublishedAt.Value,
                    Text = item.r.Snippet.TextDisplay,
                    RemoteId = item.r.Id
                }, o: item.r))
                .ToArray();


            return topLevel.Select(o => o.ytc).Concat(subLevels.Select(o => o.ytc))
                .ToArray();
        }

        public void Dispose()
        {
            _service?.Dispose();
        }

        public async Task<YoutubePlaylist[]> GetPlaylists(string channelId)
        {
            var request = _service.Playlists.List("snippet");
            request.ChannelId = channelId;

            var response = await request.ExecuteAsync();
            ApiCallCosts.Enqueue((DateTime.UtcNow, 1 + 2));

            return response.Items.Select(o => new YoutubePlaylist
            {
                RemoteId = o.Id,
                Name = o.Snippet.Localized.Title,
                PublishDate = o.Snippet.PublishedAt.Value
            }).ToArray();
        }

        public async Task<string[]> GetPlaylistVideoIds(string playlistRemoteId)
        {
            var request = _service.PlaylistItems.List("snippet");
            request.PlaylistId = playlistRemoteId;

            var response = await request.ExecuteAsync();
            ApiCallCosts.Enqueue((DateTime.UtcNow, 1 + 2));

            return response.Items.Select(o => o.Snippet.ResourceId.VideoId).ToArray();
        }
    }
}
