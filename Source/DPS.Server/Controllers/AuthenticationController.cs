﻿using System.Threading.Tasks;
using DPS.Server.Auth;
using DPS.Server.DataLayer.Models;
using DPS.Server.Dtos;
using DPS.Server.Logic;
using DPS.Server.Logic.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace DPS.Server.Controllers
{
    [ApiController]
    [Route("api/auth")]
    public class AuthenticationController : AuthenticatedController
    {
        private readonly AccountLogic _accountLogic;
        private readonly AuthenticationCryptography _authenticationCryptography;

        public AuthenticationController(AccountLogic accountLogic, AuthenticationCryptography authenticationCryptography)
        {
            _accountLogic = accountLogic;
            _authenticationCryptography = authenticationCryptography;
        }

        [HttpPost("refresh")]
        public async Task<IActionResult> Refresh([FromBody] AuthenticationTokenDto oldToken)
        {
            var result = await _authenticationCryptography.Decrypt<int>(oldToken.AuthToken);
            if (result.Failed) return Unauthorized();

            var account = await _accountLogic.GetActiveById(result.Data);
            if (account == null) return Unauthorized();

            await _accountLogic.UpdateLastSignIn(account.Id);

            return await CreateToken(account);
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] SignInDto request)
        {
            var account = await _accountLogic.GetActiveByCredentials(request.Email, request.Password);
            if (account == null) return Unauthorized();

            await _accountLogic.UpdateLastSignIn(account.Id);

            return await CreateToken(account);
        }

        private async Task<IActionResult> CreateToken(Account account)
        {
            var result = await _authenticationCryptography.Encrypt(account.Id);

            var token = new AuthenticationTokenDto();
            token.Id = account.Id;
            token.AuthToken = result.Data;
            token.RefreshAvailableFrom = result.RefreshAvailableFrom.ToUnixMillisecond();
            token.RefreshAvailableUntil = result.RefreshAvailableUntil.ToUnixMillisecond();

            return Ok(token);
        }
    }
}
