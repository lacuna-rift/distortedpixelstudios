﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DPS.Server.DataLayer.Repositories;
using DPS.Server.Mappers;
using Microsoft.AspNetCore.Mvc;

namespace DPS.Server.Controllers
{
    [ApiController]
    [Route("api/youtube")]
    public class YoutubeController : ControllerBase
    {
        private readonly YoutubeRepository _repository;

        public YoutubeController(YoutubeRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("playlists")]
        public async Task<IActionResult> GetPlaylists()
        {
            var playlists = await _repository.GetPlaylists();
            if (playlists == null) return NotFound();

            return Ok(playlists.Select(Mapper.MapYoutubePlaylist));
        }

        [HttpGet]
        [Route("videos")]
        public async Task<IActionResult> GetVideos()
        {
            var videos = await _repository.GetVideos();
            if (videos == null) return NotFound();

            var publicVideos = videos.Where(o => o.Public);
            var mapped = publicVideos.Select(Mapper.MapYoutubeVideo);
            return Ok(mapped);
        }

        [HttpGet]
        [Route("video/{remoteId}")]
        public async Task<IActionResult> GetVideoByRemoteId(string remoteId)
        {
            var video = await _repository.GetVideoByRemoteId(remoteId);
            if (video == null) return NotFound();
            if (!video.Public) return Forbid();

            var mapped = Mapper.MapYoutubeVideo(video);
            return Ok(mapped);
        }

        [HttpGet]
        [Route("video/{remoteId}/downloadables")]
        public async Task<IActionResult> GetVideoDownloadables(string remoteId)
        {
            var downloadable = await _repository.GetVideoDownloadables(remoteId);
            if (downloadable == null) return NotFound();

            var mapped = downloadable.Select(Mapper.MapYoutubeVideoDownloadables);
            return Ok(mapped);
        }

        [HttpGet]
        [Route("video/{remoteId}/comments")]
        public async Task<IActionResult> GetVideoComments(string remoteId)
        {
            var comments = await _repository.GetVideoComments(remoteId);
            if (comments == null) return NotFound();

            var mapped = Mapper.MapYoutubeVideoComments(comments);
            return Ok(mapped);
        }

        [HttpGet]
        [Route("video/{remoteId}/faqs")]
        public async Task<IActionResult> GetVideoFaqs(string remoteId)
        {
            var faqs = await _repository.GetVideoFaqs(remoteId);
            if (faqs == null) return NotFound();

            var mapped = faqs.Select(Mapper.MapYoutubeVideoFaq);
            return Ok(mapped);
        }

        [HttpGet]
        [Route("video/{remoteId}/downloadables/{handle}")]
        public async Task<IActionResult> DownloadVideoDownloadable(string remoteId, string handle)
        {
            var downloadable = await _repository.GetVideoDownloadable(remoteId, handle);
            if (downloadable == null) return NotFound();
            if (!downloadable.Visible) return Forbid();

            await _repository.IncrementDownload(downloadable);

            var data = await System.IO.File.ReadAllBytesAsync(downloadable.Payload);
            return File(data, "application/octet-stream", Path.GetFileName(downloadable.Payload));
        }

        [HttpGet]
        [Route("suggestions")]
        public async Task<IActionResult> GetSuggestions()
        {
            var suggestions = await _repository.GetSuggestions();
            if (suggestions == null) return NotFound();

            return Ok(suggestions.Select(Mapper.MapYoutubeSuggestion));
        }
    }
}
