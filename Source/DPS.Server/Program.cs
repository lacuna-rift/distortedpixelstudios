using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace DPS.Server
{
    public class Program
    {
        public const string ApplicationName = "DPSServer";

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>().UseUrls("http://+:8555");
                });
    }
}
