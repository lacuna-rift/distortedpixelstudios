﻿using System;
using Lacuna.Core.Config.Model;

namespace DPS.Server.Config
{
    public class EnvironmentConfig : ConfigSet<EnvironmentConfig>
    {
        public bool RunDevDataScript { get; set; }

        public EnvironmentConfig() : base("EnvironmentConfig", TimeSpan.FromMinutes(1))
        {
        }

        public override EnvironmentConfig GetDefault()
        {
            return new EnvironmentConfig
            {
                RunDevDataScript = false,
            };
        }
    }
}