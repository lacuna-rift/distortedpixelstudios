﻿using System;
using System.Collections.Generic;
using Lacuna.Core.Config.Model;

namespace DPS.Server.Config
{
    public class YoutubeStatUpdateConfig : ConfigSet<YoutubeStatUpdateConfig>
    {
        public Dictionary<string, bool> ChannelsToScan { get; set; }

        public YoutubeStatUpdateConfig() : base("YoutubeStatUpdate", TimeSpan.FromSeconds(5))
        {
        }

        public override YoutubeStatUpdateConfig GetDefault()
        {
            return new YoutubeStatUpdateConfig
            {
                ChannelsToScan = new Dictionary<string, bool>
                {
                    ["UCO8iVbK5YAT_Gmd41WwzJ2w"] = true,  // Our Channel
                    ["UCYbK_tjZ2OrIZFBvU6CCMiA"] = false, // Brackeys
                    ["UCmtyQOKKmrMVaKuRXz02jbQ"] = false, // Sebastian Lague
                    ["UC9Z1XWw1kmnvOOFsj6Bzy2g"] = false, // Blackthornprod 
                    ["UCtQPCnbIB7SP_gM1Xtv8bDQ"] = false, // N3K EN 
                    ["UCFK6NCbuCIVzA6Yj1G_ZqCg"] = false, // Code Monkey 
                }
            };
        }
    }
}