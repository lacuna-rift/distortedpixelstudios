﻿using System;
using Lacuna.Core.Config.Model;

namespace DPS.Server.Config
{
    public class YoutubeVideoDownloadablesConfig : ConfigSet<YoutubeVideoDownloadablesConfig>
    {
        public string DataDirectory { get; set; }

        public YoutubeVideoDownloadablesConfig() : base("YoutubeVideoDownloadables", TimeSpan.FromSeconds(5))
        {
        }

        public override YoutubeVideoDownloadablesConfig GetDefault()
        {
            return new YoutubeVideoDownloadablesConfig
            {
                DataDirectory= "../../Downloadables"
            };
        }
    }
}