﻿namespace DPS.Server.Dtos
{
    public class YoutubeVideoCommentDto
    {
        public string RemoteId { get; set; }
        public string Author { get; set; }
        public string Thumbnail { get; set; }
        public string Text { get; set; }
        public long Timestamp { get; set; }
        public int Likes { get; set; }
        public YoutubeVideoCommentDto[] Replies { get; set; }

        public long DisplayOrder { get; set; }
    }
}