﻿namespace DPS.Server.Dtos
{
    public class AuthenticationTokenDto
    {
        public int Id { get; set; }
        public string AuthToken { get; set; }
        public long RefreshAvailableFrom { get; set; }
        public long RefreshAvailableUntil { get; set; }
    }
}
