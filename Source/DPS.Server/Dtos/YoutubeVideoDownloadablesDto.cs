﻿using DPS.Server.DataLayer.Models.Enums;

namespace DPS.Server.Dtos
{
    public class YoutubeVideoDownloadablesDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Handle { get; set; }
        public YoutubeVideoDownloadableTypes Type { get; set; }

        public long DisplayOrder { get; set; }
    }
}