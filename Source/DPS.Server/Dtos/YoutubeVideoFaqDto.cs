﻿namespace DPS.Server.Dtos
{
    public class YoutubeVideoFaqDto
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public short VideoId { get; set; }

        public long DisplayOrder { get; set; }
    }
}