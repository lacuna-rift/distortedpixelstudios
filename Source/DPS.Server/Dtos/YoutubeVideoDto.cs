﻿namespace DPS.Server.Dtos
{
    public class YoutubeVideoDto
    {
        public short Id { get; set; }
        public string RemoteId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public short PlaylistId { get; set; }

        public int Likes { get; set; }
        public int Views { get; set; }
        public int Comments { get; set; }
        public int Downloadables { get; set; }

        public long DisplayOrder { get; set; }
        public long PublishDate { get; set; }
    }
}