﻿namespace DPS.Server.Dtos
{
    public class YoutubeSuggestionDto
    {
        public short Id { get; set; }
        public string Name { get; set; }
        public short Votes { get; set; }

        public long DisplayOrder { get; set; }
    }
}