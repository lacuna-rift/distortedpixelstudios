﻿namespace DPS.Server.Dtos
{
    public class YoutubePlaylistDto
    {
        public short Id { get; set; }
        public string RemoteId { get; set; }
        public string Name { get; set; }

        public long DisplayOrder { get; set; }
    }
}