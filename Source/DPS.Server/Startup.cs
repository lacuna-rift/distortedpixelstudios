using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DPS.Server.Auth;
using DPS.Server.Config;
using DPS.Server.DataLayer.Models;
using DPS.Server.DataLayer.Models.Enums;
using DPS.Server.DataLayer.Repositories;
using DPS.Server.Initializers;
using DPS.Server.Logic;
using DPS.Server.Workers;
using FluentNHibernate;
using Lacuna.Core.Config.Extensions;
using Lacuna.Core.Data;
using Lacuna.Core.Data.Extensions;
using Lacuna.Core.Data.Mapping;
using Lacuna.Core.Extensions;
using Lacuna.Core.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DPS.Server
{
    public class Startup
    {
        private const string DevOrigins = "_DevOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(DevOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:4200")
                            .AllowCredentials()
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    });
            });

            services.AddScoped<YoutubeStatUpdateWorker>();
            services.AddScoped<DownloadablesWorker>();
            //services.AddScoped<IEnvironmentInitializer, DevDatabaseWipeInitializer>();
            services.AddScoped<IEnvironmentInitializer, DevDataInitializer>();

            services.RegisterAsMappingAssembly(GetType().Assembly);
            services.RegisterAsMigrationAssembly(GetType().Assembly);
            services.RegisterModule(new DataModule(Program.ApplicationName));

            ////////////////////////////////////////////////////////////////
            /////>  CONFIGS
            ////////////////////////////////////////////////////////////////

            services.RegisterConfig<EnvironmentConfig>();
            services.RegisterConfig<YoutubeStatUpdateConfig>();
            services.RegisterConfig<YoutubeVideoDownloadablesConfig>();

            ////////////////////////////////////////////////////////////////
            /////>  ENUMS
            ////////////////////////////////////////////////////////////////

            services.RegisterAutoMappedEnum<AccountState>();
            services.RegisterAutoMappedEnum<YoutubeVideoDownloadableTypes>();

            ////////////////////////////////////////////////////////////////
            /////>  TABLES
            ////////////////////////////////////////////////////////////////

            services.RegisterAutoMappedType<Account>();
            services.RegisterAutoMappedType<YoutubeCredential>();
            services.RegisterAutoMappedType<YoutubeRawStat>();

            services.RegisterAutoMappedType<YoutubePlaylist>();
            services.RegisterAutoMappedType<YoutubeVideo>();
            services.RegisterAutoMappedType<YoutubeVideoComment>();
            services.RegisterAutoMappedType<YoutubeVideoDownloadables>();
            services.RegisterAutoMappedType<YoutubeVideoFaq>();
            services.RegisterAutoMappedType<YoutubePulledStat>();
            services.RegisterAutoMappedType<YoutubeSuggestion>();

            ////////////////////////////////////////////////////////////////
            /////>  Repositories
            ////////////////////////////////////////////////////////////////

            services.AddScoped<AccountRepository>();
            services.AddScoped<YoutubeRepository>();

            ////////////////////////////////////////////////////////////////
            /////>  Logic
            ////////////////////////////////////////////////////////////////

            services.AddScoped<AuthenticationCryptography>();
            services.AddScoped<AccountLogic>();

            ////////////////////////////////////////////////////////////////

            services.AddResponseCompression();
            services.AddFhAuthenticationAndAuthorization();
            services.AddMvc();//.AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseResponseCompression();
            app.UseAuthentication();
            app.UseAuthorization();

            var application = app.ApplicationServices.GetService<IApplication>();
            application.Initialize(Program.ApplicationName, app.ApplicationServices);

            using (var scope = app.ApplicationServices.CreateScope())
            {
                EnsureAllEnumsAreMappedToAppeaseTheDes(scope);
                var services = scope.ServiceProvider.GetServices<IEnvironmentInitializer>();
                foreach (var initializer in services.OrderBy(o => o.Order))
                {
                    initializer.Initialize();
                }
            }
            // free up the memory claimed by debug stuff
            AutoLacunaClassMapMigrator.Clear();
            GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true, true);

            app.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<YoutubeStatUpdateWorker>().Start();
            app.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<DownloadablesWorker>().Start();


            if (env.IsDevelopment())
            {
                app.UseCors(DevOrigins);
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void EnsureAllEnumsAreMappedToAppeaseTheDes(IServiceScope scope)
        {
            var hasErrors = false;
            var registeredEnums = new HashSet<Type>();
            foreach (var mappingProvider in scope.ServiceProvider.GetServices<IMappingProvider>())
            {
                var providerType = mappingProvider.GetType();
                var mappedType = providerType.GetGenericArguments()[0];

                if (mappedType.IsEnum) registeredEnums.Add(mappedType);
                else
                {
                    foreach (var propertyInfo in mappedType.GetProperties(BindingFlags.Instance | BindingFlags.Public))
                    {
                        if (propertyInfo.PropertyType.IsEnum && registeredEnums.Add(propertyInfo.PropertyType))
                        {
                            Console.WriteLine($"Type Validation Failed : Enum [{propertyInfo.PropertyType.Name}] is not registered with [RegisterAutoMappedEnum]!");
                            hasErrors = true;
                        }
                    }
                }
            }

            if (hasErrors)
            {
                throw new Exception("Fix the validation errors (Check the console app output)");
            }
        }
    }
}
