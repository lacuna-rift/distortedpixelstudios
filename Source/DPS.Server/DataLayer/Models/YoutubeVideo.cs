﻿using System;
using Lacuna.Core.Data.Mapping;

namespace DPS.Server.DataLayer.Models
{
    public class YoutubeVideo
    {
        public virtual short Id { get; set; }
        [StringLength(11)]
        public virtual string RemoteId { get; set; }
        [Reference]
        public virtual YoutubePlaylist Playlist { get; set; }

        [StringLength(255)]
        public virtual string Name { get; set; }
        [StringLength(5000)]
        public virtual string Description { get; set; }
        [StringLength(5000)]
        public virtual string DisplayDescription { get; set; }
        [StringLength(255)]
        public virtual string Thumbnail { get; set; }
        public virtual bool Public { get; set; }
        public virtual DateTime PublishDate { get; set; }

        public virtual int Likes { get; set; }
        public virtual int Views { get; set; }
        public virtual int Comments { get; set; }
        public virtual int Downloadables { get; set; }

        public virtual long DisplayOrder { get; set; }
    }
}
