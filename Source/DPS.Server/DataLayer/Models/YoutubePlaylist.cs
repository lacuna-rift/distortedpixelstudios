﻿using System;
using Lacuna.Core.Data.Mapping;

namespace DPS.Server.DataLayer.Models
{
    public class YoutubePlaylist
    {
        public virtual short Id { get; set; }

        [StringLength(64)]
        public virtual string RemoteId { get; set; }

        [StringLength(255)]
        public virtual string Name { get; set; }

        public virtual DateTime PublishDate { get; set; }

        public virtual long DisplayOrder { get; set; }
    }
}