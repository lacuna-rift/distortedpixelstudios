﻿using System;
using Lacuna.Core.Data.Mapping;
using Newtonsoft.Json;

namespace DPS.Server.DataLayer.Models
{
    public class YoutubeCredential
    {
        [JsonIgnore]
        public virtual int Id { get; set; }

        [JsonIgnore]
        public virtual DateTime LockedUntil { get; set; }

        [JsonProperty("type")]
        [StringLength(100)]
        public virtual string Type { get; set; }

        [JsonProperty("project_id")]
        [StringLength(100)]
        public virtual string ProjectId { get; set; }

        [JsonProperty("private_key_id")]
        [StringLength(100)]
        public virtual string PrivateKeyId { get; set; }

        [JsonProperty("private_key")]
        [StringLength(4000)]
        public virtual string PrivateKey { get; set; }

        [JsonProperty("client_email")]
        [StringLength(100)]
        public virtual string ClientEmail { get; set; }

        [JsonProperty("client_id")]
        [StringLength(100)]
        public virtual string ClientId { get; set; }

        [JsonProperty("auth_uri")]
        [StringLength(100)]
        public virtual string AuthUri { get; set; }

        [JsonProperty("token_uri")]
        [StringLength(100)]
        public virtual string TokenUri { get; set; }

        [JsonProperty("auth_provider_x509_cert_url")]
        [StringLength(100)]
        public virtual string AuthProviderX509CertUrl { get; set; }

        [JsonProperty("client_x509_cert_url")]
        [StringLength(200)]
        public virtual string ClientX509CertUrl { get; set; }
    }
}
