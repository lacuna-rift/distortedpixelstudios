﻿using System;
using DPS.Server.DataLayer.Models.Enums;
using Lacuna.Core.Data.Mapping;

namespace DPS.Server.DataLayer.Models
{
    public class Account
    {
        public virtual int Id { get; set; }
        public virtual DateTime Creation { get; set; }
        public virtual DateTime LastSignin { get; set; }

        public virtual AccountState State { get; set; }

        [StringLength(255)]
        public virtual string DisplayName { get; set; }
        [StringLength(255)]
        public virtual string Email { get; set; }
        [StringLength(64)]
        public virtual string PasswordHash { get; set; }
    }
}