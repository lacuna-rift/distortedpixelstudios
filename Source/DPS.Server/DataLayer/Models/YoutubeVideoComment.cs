﻿using System;
using Lacuna.Core.Data.Mapping;

namespace DPS.Server.DataLayer.Models
{
    public class YoutubeVideoComment
    {
        public virtual int Id { get; set; }

        [StringLength(64)]
        public virtual string RemoteId { get; set; }

        [StringLength(255)]
        public virtual string Author { get; set; }
        [StringLength(255)]
        public virtual string Thumbnail { get; set; }
        [StringLength(5000)]
        public virtual string Text { get; set; }
        public virtual DateTime PublishDate { get; set; }

        public virtual int Likes { get; set; }

        [Reference]
        public virtual YoutubeVideo Video { get; set; }
        [Nullable, Reference]
        public virtual YoutubeVideoComment ReplyTo { get; set; }

        public virtual long DisplayOrder { get; set; }
    }
}