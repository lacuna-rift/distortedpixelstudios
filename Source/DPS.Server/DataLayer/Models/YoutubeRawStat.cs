﻿using System;
using Lacuna.Core.Data.Mapping;

namespace DPS.Server.DataLayer.Models
{
    public class YoutubeRawStat
    {
        public virtual long Id { get; set; }
        public virtual DateTime Timestamp { get; set; }
        [StringLength(100)]
        public virtual string ChannelId { get; set; }
        [StringLength(100)]
        public virtual string ChannelName { get; set; }
        public virtual int ChannelSubs { get; set; }
        [StringLength(100)]
        public virtual string VideoId { get; set; }
        [StringLength(100)]
        public virtual string VideoName { get; set; }
        public virtual DateTime VideoDate { get; set; }
        public virtual int CommentCount { get; set; }
        public virtual int DislikeCount { get; set; }
        public virtual int FavoriteCount { get; set; }
        public virtual int LikeCount { get; set; }
        public virtual int ViewCount { get; set; }
    }
}