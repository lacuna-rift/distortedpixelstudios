﻿using Lacuna.Core.Data.Mapping;

namespace DPS.Server.DataLayer.Models
{
    public class YoutubeVideoFaq
    {
        public virtual short Id { get; set; }

        [StringLength(5000)]
        public virtual string Question { get; set; }
        [StringLength(5000)]
        public virtual string Answer { get; set; }
        [Reference]
        public virtual YoutubeVideo Video { get; set; }

        public virtual long DisplayOrder { get; set; }
    }
}