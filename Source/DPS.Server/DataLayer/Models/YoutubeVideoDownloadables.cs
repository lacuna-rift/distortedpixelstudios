﻿using DPS.Server.DataLayer.Models.Enums;
using Lacuna.Core.Data.Mapping;

namespace DPS.Server.DataLayer.Models
{
    public class YoutubeVideoDownloadables
    {
        public virtual int Id { get; set; }

        [StringLength(255)]
        public virtual string Name { get; set; }
        [StringLength(5000)]
        public virtual string Description { get; set; }
        [StringLength(255)]
        public virtual string Payload { get; set; } // Full File Path
        [StringLength(255)]
        public virtual string Handle { get; set; }
        
        public virtual int Downloads { get; set; }
        public virtual bool Visible { get; set; }

        [Reference]
        public virtual YoutubeVideo Video { get; set; }
        public virtual YoutubeVideoDownloadableTypes Type { get; set; }

        public virtual long DisplayOrder { get; set; }
    }
}