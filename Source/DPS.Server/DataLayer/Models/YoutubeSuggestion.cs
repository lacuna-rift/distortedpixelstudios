﻿using System;
using Lacuna.Core.Data.Mapping;

namespace DPS.Server.DataLayer.Models
{
    public class YoutubeSuggestion
    {
        public virtual short Id { get; set; }

        [StringLength(255)]
        public virtual string Name { get; set; }
        public virtual short Votes { get; set; }
        [Nullable]
        public virtual DateTime CompletedDate { get; set; }

        public virtual long DisplayOrder { get; set; }
    }
}