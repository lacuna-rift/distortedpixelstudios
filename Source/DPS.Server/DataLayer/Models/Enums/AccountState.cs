﻿namespace DPS.Server.DataLayer.Models.Enums
{
    public enum AccountState
    {
        /// <summary>
        /// Still awaiting <see cref="Account.Email"/> to be verified
        /// </summary>
        Pending = 0,

        /// <summary>
        /// Standard state
        /// </summary>
        Active = 1,

        /// <summary>
        /// Used when the account has been locked by an admin for various reasons
        /// </summary>
        Disabled = 2,

        /// <summary>
        /// The user has "deleted" their account 
        /// </summary>
        Deleted = 3
    }
}