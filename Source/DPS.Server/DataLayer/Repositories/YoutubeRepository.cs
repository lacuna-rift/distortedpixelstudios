﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DPS.Server.DataLayer.Models;
using Lacuna.Core.Data.Extensions;
using Lacuna.Core.Data.Persistance.Interfaces;
using NHibernate.Linq;

namespace DPS.Server.DataLayer.Repositories
{
    public class YoutubeRepository
    {
        private readonly ISession _session;

        public YoutubeRepository(ISession session)
        {
            _session = session;
        }

        public Task<List<YoutubePlaylist>> GetPlaylists()
        {
            return _session.Query<YoutubePlaylist>()
                .OrderBy(o => o.DisplayOrder)
                .ToListAsync();
        }

        public Task<List<YoutubeVideo>> GetVideos()
        {
            return _session.Query<YoutubeVideo>()
                .OrderBy(o => o.DisplayOrder)
                .ToListAsync();
        }

        public Task<YoutubeVideo> GetVideoByRemoteId(string remoteId)
        {
            return _session.Query<YoutubeVideo>()
                .SingleOrDefaultAsync(o => o.RemoteId == remoteId);
        }

        public Task<List<YoutubeVideoDownloadables>> GetVideoDownloadables(string remoteId)
        {
            return _session.Query<YoutubeVideoDownloadables>()
                .Where(o => o.Video.RemoteId == remoteId)
                .OrderBy(o => o.DisplayOrder)
                .ToListAsync();
        }

        public Task<YoutubeVideoDownloadables> GetVideoDownloadable(string remoteId, string handle)
        {
            return _session.Query<YoutubeVideoDownloadables>()
                .Where(o => o.Video.RemoteId == remoteId)
                .Where(o => o.Handle == handle)
                .SingleOrDefaultAsync();
        }

        public Task<List<YoutubeVideoComment>> GetVideoComments(string remoteId)
        {
            return _session.Query<YoutubeVideoComment>()
                .Where(o => o.Video.RemoteId == remoteId)
                .OrderBy(o => o.DisplayOrder)
                .ToListAsync();
        }

        public Task<List<YoutubeVideoFaq>> GetVideoFaqs(string remoteId)
        {
            return _session.Query<YoutubeVideoFaq>()
                .Where(o => o.Video.RemoteId == remoteId)
                .OrderBy(o => o.DisplayOrder)
                .ToListAsync();
        }

        public Task<List<YoutubeSuggestion>> GetSuggestions()
        {
            return _session.Query<YoutubeSuggestion>()
                .Where(o => o.CompletedDate == null)
                .OrderByDescending(o => o.Votes)
                .ThenBy(o => o.DisplayOrder)
                .ToListAsync();
        }

        public Task IncrementDownload(YoutubeVideoDownloadables downloadable)
        {
            downloadable.Downloads++;
            return _session.Query<YoutubeVideoDownloadables>()
                .Where(o => o.Id == downloadable.Id)
                .UpdateSet(o => o.Downloads, o => o.Downloads + 1)
                .ExecuteUpdateAsync();
        }
    }
}
