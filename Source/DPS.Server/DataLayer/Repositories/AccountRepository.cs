using System;
using System.Linq;
using System.Threading.Tasks;
using DPS.Server.DataLayer.Models;
using DPS.Server.DataLayer.Models.Enums;
using Lacuna.Core.Data.Extensions;
using Lacuna.Core.Data.Persistance.Interfaces;
using NHibernate.Linq;

namespace DPS.Server.DataLayer.Repositories
{
    public class AccountRepository
    {
        private readonly IUnitOfWork _db;

        public AccountRepository(IUnitOfWork db)
        {
            _db = db;
        }

        public Task<Account> Get(int accountId)
        {
            return _db.Query<Account>()
                .Where(o => o.Id == accountId)
                .SingleOrDefaultAsync();
        }

        public Task<Account> Get(string email, string passwordHash)
        {
            return _db.Query<Account>()
                .Where(o => o.Email == email)
                .Where(o => o.PasswordHash == passwordHash)
                .SingleOrDefaultAsync();
        }

        public Task Create(Account account)
        {
            return _db.InsertAsync(account);
        }

        public Task<int?> GetAccountIdForEmail(string email)
        {
            return _db.Query<Account>()
                .Where(o => o.Email == email)
                .Select(o => (int?)o.Id)
                .SingleOrDefaultAsync();
        }

        public Task<int> UpdateState(int accountId, in AccountState state)
        {
            return _db.Query<Account>()
                .Where(o => o.Id == accountId)
                .UpdateSet(o => o.State, state)
                .ExecuteUpdateAsync();
        }

        public Task<int> UpdatePassword(int accountId, in string passwordHash)
        {
            return _db.Query<Account>()
                .Where(o => o.Id == accountId)
                .UpdateSet(o => o.PasswordHash, passwordHash)
                .ExecuteUpdateAsync();
        }

        public Task<int> UpdateLastSignIn(int accountId, in DateTime timestamp)
        {
            return _db.Query<Account>()
                .Where(o => o.Id == accountId)
                .UpdateSet(o => o.LastSignin, timestamp)
                .ExecuteUpdateAsync();
        }
    }
}