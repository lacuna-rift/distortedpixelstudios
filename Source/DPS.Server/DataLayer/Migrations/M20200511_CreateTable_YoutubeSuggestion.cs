using System.Diagnostics.CodeAnalysis;
using Lacuna.Core.Data.Migrator;

namespace DPS.Server.DataLayer.Migrations
{
    [LacunaMigration(2020, 05, 11, 17, 00)]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    internal class M20200511_CreateTable_YoutubeSuggestion : LacunaMigration
    {
        public override void Up()
        {
            Create.Table("YoutubeSuggestion")
                .WithColumn("Id").AsInt16().NotNullable().Identity().PrimaryKey()
                .WithColumn("Name").AsString(255).NotNullable()
                .WithColumn("Votes").AsInt16().NotNullable()
                .WithColumn("CompletedDate").AsDateTime().Nullable();
        }
    }
}