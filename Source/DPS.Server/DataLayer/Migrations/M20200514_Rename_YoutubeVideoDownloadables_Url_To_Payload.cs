using System.Diagnostics.CodeAnalysis;
using Lacuna.Core.Data.Migrator;

namespace DPS.Server.DataLayer.Migrations
{
    [LacunaMigration(2020, 05, 14, 22, 58)]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    internal class M20200514_Rename_YoutubeVideoDownloadables_Url_To_Payload : LacunaMigration
    {
        public override void Up()
        {
            Rename.Column("Url").OnTable("YoutubeVideoDownloadables").To("Payload");

            Alter.Table("YoutubeVideoDownloadables").AddColumn("Downloads").AsInt32().NotNullable().WithDefaultValue(0);
            Alter.Table("YoutubeVideoDownloadables").AddColumn("Visible").AsBoolean().NotNullable().WithDefaultValue(true);
            Alter.Table("YoutubeVideoDownloadables").AddColumn("Handle").AsString(32).NotNullable().WithDefaultValue("");
        }
    }
}