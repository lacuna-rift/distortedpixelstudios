// AUTO-GEN HASH : C717A25E7B72F1E39657B88E6BFB5B440C9EA398016AF84D4150B9F7E706025A
using System.Diagnostics.CodeAnalysis;
using Lacuna.Core.Data.Mapping;
using Lacuna.Core.Data.Migrator;

namespace DPS.Server.DataLayer.Migrations
{
    [LacunaMigration(2020, 05, 10, 00, 00)]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    internal class M20200510_InitialDatabaseStructure : LacunaMigration
    {
        public override void Up()
        {
            var migrations = AutoLacunaClassMapMigrator.All;
            //Debugger.Break();
            // This file is auto-generated by setting [LAST_FORCED_WIPE] in [DevDatabaseWipeInitializer]

               Execute.Sql("CREATE SCHEMA IF NOT EXISTS enums");
   Create.Table("AccountState")
      .InSchema("enums")
      .WithColumn("Id").AsInt64().NotNullable().PrimaryKey()
      .WithColumn("FieldName").AsString(100).NotNullable()
      .WithColumn("DisplayName").AsString(200).NotNullable()
      .WithColumn("Description").AsString(500).NotNullable();

Delete.FromTable("AccountState").InSchema("enums").AllRows();
   Insert.IntoTable("AccountState").InSchema("enums")
      .Row(new { Id = 0, FieldName = "Pending", DisplayName = "Pending", Description = ""})
      .Row(new { Id = 1, FieldName = "Active", DisplayName = "Active", Description = ""})
      .Row(new { Id = 2, FieldName = "Disabled", DisplayName = "Disabled", Description = ""})
      .Row(new { Id = 3, FieldName = "Deleted", DisplayName = "Deleted", Description = ""});

Execute.Sql("CREATE SCHEMA IF NOT EXISTS enums");
   Create.Table("YoutubeVideoDownloadableTypes")
      .InSchema("enums")
      .WithColumn("Id").AsInt64().NotNullable().PrimaryKey()
      .WithColumn("FieldName").AsString(100).NotNullable()
      .WithColumn("DisplayName").AsString(200).NotNullable()
      .WithColumn("Description").AsString(500).NotNullable();

Delete.FromTable("YoutubeVideoDownloadableTypes").InSchema("enums").AllRows();
   Insert.IntoTable("YoutubeVideoDownloadableTypes").InSchema("enums")
      .Row(new { Id = 0, FieldName = "UnityPackage", DisplayName = "UnityPackage", Description = ""});

Create.Table("Account")
      .WithColumn("Id").AsInt32().NotNullable().Identity().PrimaryKey()
      .WithColumn("Creation").AsDateTime().NotNullable()
      .WithColumn("LastSignin").AsDateTime().NotNullable()
      .WithColumn("State").AsInt32().NotNullable()
      .WithColumn("DisplayName").AsString(255).NotNullable()
      .WithColumn("Email").AsString(255).NotNullable()
      .WithColumn("PasswordHash").AsString(64).NotNullable();

Create.Table("YoutubeCredential")
      .WithColumn("Id").AsInt32().NotNullable().Identity().PrimaryKey()
      .WithColumn("LockedUntil").AsDateTime().NotNullable()
      .WithColumn("Type").AsString(100).NotNullable()
      .WithColumn("ProjectId").AsString(100).NotNullable()
      .WithColumn("PrivateKeyId").AsString(100).NotNullable()
      .WithColumn("PrivateKey").AsString(4000).NotNullable()
      .WithColumn("ClientEmail").AsString(100).NotNullable()
      .WithColumn("ClientId").AsString(100).NotNullable()
      .WithColumn("AuthUri").AsString(100).NotNullable()
      .WithColumn("TokenUri").AsString(100).NotNullable()
      .WithColumn("AuthProviderX509CertUrl").AsString(100).NotNullable()
      .WithColumn("ClientX509CertUrl").AsString(200).NotNullable();

Create.Table("YoutubeRawStat")
      .WithColumn("Id").AsInt64().NotNullable().Identity().PrimaryKey()
      .WithColumn("Timestamp").AsDateTime().NotNullable()
      .WithColumn("ChannelId").AsString(100).NotNullable()
      .WithColumn("ChannelName").AsString(100).NotNullable()
      .WithColumn("ChannelSubs").AsInt32().NotNullable()
      .WithColumn("VideoId").AsString(100).NotNullable()
      .WithColumn("VideoName").AsString(100).NotNullable()
      .WithColumn("VideoDate").AsDateTime().NotNullable()
      .WithColumn("CommentCount").AsInt32().NotNullable()
      .WithColumn("DislikeCount").AsInt32().NotNullable()
      .WithColumn("FavoriteCount").AsInt32().NotNullable()
      .WithColumn("LikeCount").AsInt32().NotNullable()
      .WithColumn("ViewCount").AsInt32().NotNullable();

Create.Table("YoutubePlaylist")
      .WithColumn("Id").AsInt16().NotNullable().Identity().PrimaryKey()
      .WithColumn("RemoteId").AsString(64).NotNullable()
      .WithColumn("Name").AsString(255).NotNullable()
      .WithColumn("PublishDate").AsDateTime().NotNullable();

Create.Table("YoutubeVideo")
      .WithColumn("Id").AsInt16().NotNullable().Identity().PrimaryKey()
      .WithColumn("RemoteId").AsString(11).NotNullable()
      .WithColumn("Name").AsString(255).NotNullable()
      .WithColumn("Description").AsString(5000).NotNullable()
      .WithColumn("Thumbnail").AsString(255).NotNullable()
      .WithColumn("Public").AsBoolean().NotNullable()
      .WithColumn("PublishDate").AsDateTime().NotNullable()
      .WithColumn("Likes").AsInt32().NotNullable()
      .WithColumn("Views").AsInt32().NotNullable()
      .WithColumn("Comments").AsInt32().NotNullable()
      .WithColumn("Downloadables").AsInt32().NotNullable()
      .WithColumn("PlaylistId").AsInt16().NotNullable().ForeignKey("YoutubePlaylist");

Create.Table("YoutubeVideoComment")
      .WithColumn("Id").AsInt32().NotNullable().Identity().PrimaryKey()
      .WithColumn("RemoteId").AsString(64).NotNullable()
      .WithColumn("Author").AsString(255).NotNullable()
      .WithColumn("Thumbnail").AsString(255).NotNullable()
      .WithColumn("Text").AsString(5000).NotNullable()
      .WithColumn("PublishDate").AsDateTime().NotNullable()
      .WithColumn("Likes").AsInt32().NotNullable()
      .WithColumn("VideoId").AsInt16().NotNullable().ForeignKey("YoutubeVideo")
      .WithColumn("ReplyToId").AsInt32().Nullable().ForeignKey("YoutubeVideoComment");

Create.Table("YoutubeVideoDownloadables")
      .WithColumn("Id").AsInt32().NotNullable().Identity().PrimaryKey()
      .WithColumn("Name").AsString(255).NotNullable()
      .WithColumn("Description").AsString(5000).NotNullable()
      .WithColumn("Url").AsString(255).NotNullable()
      .WithColumn("Type").AsByte().NotNullable()
      .WithColumn("VideoId").AsInt16().NotNullable().ForeignKey("YoutubeVideo");

Create.Table("YoutubeVideoFaq")
      .WithColumn("Id").AsInt16().NotNullable().Identity().PrimaryKey()
      .WithColumn("Question").AsString(5000).NotNullable()
      .WithColumn("Answer").AsString(5000).NotNullable()
      .WithColumn("VideoId").AsInt16().NotNullable().ForeignKey("YoutubeVideo");

Create.Table("YoutubePulledStat")
      .WithColumn("Id").AsInt64().NotNullable().Identity().PrimaryKey()
      .WithColumn("Timestamp").AsDateTime().NotNullable()
      .WithColumn("ChannelId").AsString(11).NotNullable()
      .WithColumn("ChannelName").AsString(255).NotNullable()
      .WithColumn("ChannelSubs").AsInt32().NotNullable()
      .WithColumn("VideoId").AsString(11).NotNullable()
      .WithColumn("VideoName").AsString(255).NotNullable()
      .WithColumn("VideoDate").AsDateTime().NotNullable()
      .WithColumn("CommentCount").AsInt32().NotNullable()
      .WithColumn("DislikeCount").AsInt32().NotNullable()
      .WithColumn("FavoriteCount").AsInt32().NotNullable()
      .WithColumn("LikeCount").AsInt32().NotNullable()
      .WithColumn("ViewCount").AsInt32().NotNullable();
        }
    }
}
