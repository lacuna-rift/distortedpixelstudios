using System.Diagnostics.CodeAnalysis;
using Lacuna.Core.Data.Migrator;

namespace DPS.Server.DataLayer.Migrations
{
    [LacunaMigration(2020, 05, 11, 17, 10)]
    [SuppressMessage("ReSharper", "UnusedMember.Global")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    internal class M20200511_AlterTable_DisplayOrder_Column : LacunaMigration
    {
        public override void Up()
        {
            Alter.Table("YoutubePlaylist").AddColumn("DisplayOrder").AsInt64().NotNullable().WithDefaultValue(0);

            Alter.Table("YoutubeSuggestion").AddColumn("DisplayOrder").AsInt64().NotNullable().WithDefaultValue(0);

            Alter.Table("YoutubeVideo")
                .AddColumn("DisplayOrder").AsInt64().NotNullable().WithDefaultValue(0)
                .AddColumn("DisplayDescription").AsString(5000).NotNullable().WithDefaultValue("");

            Alter.Table("YoutubeVideoComment").AddColumn("DisplayOrder").AsInt64().NotNullable().WithDefaultValue(0);

            Alter.Table("YoutubeVideoDownloadables").AddColumn("DisplayOrder").AsInt64().NotNullable().WithDefaultValue(0);

            Alter.Table("YoutubeVideoFaq").AddColumn("DisplayOrder").AsInt64().NotNullable().WithDefaultValue(0);
        }
    }
}