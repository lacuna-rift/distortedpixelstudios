﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DPS.Server.DataLayer.Models;
using DPS.Server.Dtos;
using DPS.Server.Logic.Extensions;

namespace DPS.Server.Mappers
{
    public static class Mapper
    {
        public static readonly Regex DescriptionRegex = new Regex(@"(https?\:\/\/\S*)\s?", RegexOptions.Multiline | RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public static YoutubePlaylistDto MapYoutubePlaylist(YoutubePlaylist raw)
        {
            return new YoutubePlaylistDto
            {
                Id = raw.Id,
                RemoteId = raw.RemoteId,
                Name = raw.Name
            };
        }

        public static YoutubeVideoCommentDto[] MapYoutubeVideoComments(IList<YoutubeVideoComment> replies)
        {
            var dic = replies.GroupBy(o => o.ReplyTo?.Id ?? 0).ToDictionary(o => o.Key, o => o.ToArray());
            return dic.GetValueOrDefault(0)?.Select(root => MapYoutubeVideoComment(root, dic.GetValueOrDefault(root.Id))).ToArray() ?? Array.Empty<YoutubeVideoCommentDto>();
        }
        public static YoutubeVideoCommentDto MapYoutubeVideoComment(YoutubeVideoComment raw, YoutubeVideoComment[] replies = null)
        {
            return new YoutubeVideoCommentDto
            {
                RemoteId = raw.RemoteId,
                Author = raw.Author,
                Thumbnail = raw.Thumbnail,
                Text = raw.Text,
                Timestamp = raw.PublishDate.ToUnixMillisecond(),
                Likes = raw.Likes,
                Replies = replies?.Select(o => MapYoutubeVideoComment(o)).OrderBy(o => -o.DisplayOrder).ToArray() ?? Array.Empty<YoutubeVideoCommentDto>(),
                DisplayOrder = raw.DisplayOrder
            };
        }

        public static YoutubeVideoDownloadablesDto MapYoutubeVideoDownloadables(YoutubeVideoDownloadables raw)
        {
            return new YoutubeVideoDownloadablesDto
            {
                Name = raw.Name,
                Description = raw.Description,
                Handle = raw.Handle,
                Type = raw.Type,
                DisplayOrder = raw.DisplayOrder
            };
        }

        public static YoutubeVideoDto MapYoutubeVideo(YoutubeVideo raw)
        {
            var desc = raw.DisplayDescription;
            var matches = DescriptionRegex.Matches(desc);
            foreach (var m in matches.ToArray().Reverse())
            {
                desc = desc.Substring(0, m.Groups[1].Index)
                       + $"<a href='{m.Groups[1].Value}' target='_blank'>{m.Groups[1].Value}</a>"
                       + desc.Substring(m.Groups[1].Index + m.Groups[1].Length);
            }

            return new YoutubeVideoDto
            {
                Id = raw.Id,
                RemoteId = raw.RemoteId,

                Name = raw.Name,
                Description = desc.Replace("\n", "<br/>"),
                Thumbnail = raw.Thumbnail,
                PlaylistId = raw.Playlist.Id,

                PublishDate = raw.PublishDate.ToUnixMillisecond(),

                Likes = raw.Likes,
                Views = raw.Views,
                Comments = raw.Comments,
                Downloadables = raw.Downloadables,
                DisplayOrder = raw.DisplayOrder
            };
        }

        public static YoutubeVideoFaqDto MapYoutubeVideoFaq(YoutubeVideoFaq raw)
        {
            return new YoutubeVideoFaqDto
            {
                Question = raw.Question,
                Answer = raw.Answer,
                VideoId = raw.Video.Id,
                DisplayOrder = raw.DisplayOrder
            };
        }

        public static YoutubeSuggestionDto MapYoutubeSuggestion(YoutubeSuggestion raw)
        {
            return new YoutubeSuggestionDto
            {
                Id = raw.Id,
                Name = raw.Name,
                Votes = raw.Votes,
                DisplayOrder = raw.DisplayOrder
            };
        }

        public static AccountDto MapAccount(Account account)
        {
            throw new NotImplementedException();
        }
    }
}