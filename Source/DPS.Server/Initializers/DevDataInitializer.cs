﻿using System;
using System.Linq;
using DPS.Server.Config;
using DPS.Server.DataLayer.Models;
using DPS.Server.DataLayer.Repositories;
using Lacuna.Core.Data.Persistance.Interfaces;
using Lacuna.Core.Interfaces;
using Newtonsoft.Json;
using NLog;

namespace DPS.Server.Initializers
{
    public class DevDataInitializer : IEnvironmentInitializer
    {
        protected static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public int Order { get; } = 1;

        private readonly EnvironmentConfig _config;
        private readonly IUnitOfWork _unitOfWork;
        private readonly YoutubeRepository _youtubeRepository;

        public DevDataInitializer(EnvironmentConfig config, IUnitOfWork unitOfWork, YoutubeRepository youtubeRepository)
        {
            _config = config;
            _unitOfWork = unitOfWork;
            _youtubeRepository = youtubeRepository;
        }

        public void Initialize()
        {
            if (!_config.RunDevDataScript) return;

            if (!_unitOfWork.Query<YoutubeCredential>().Any())
            {
                InsertYoutubeCredential("{ \"type\": \"service_account\", \"project_id\": \"youtube-275008\", \"private_key_id\": \"61c429b1354a421f486c8d08e3e511da88c02664\", \"private_key\": \"-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCuGvYo5CjZJLX+\nW0JzAY0h2zS8bpxNlKPlmxEq1QXhXS5zyVgiu6JCp0YCj/ueaxq/+a9jIP0oWTZE\ngRC5u5iiMq34TDrpVwCLjMu3PPipXqecTScW1f+lmDy0KFh+hYyrmy0XgyTXAqJQ\nJR2oa6O3yIstPVqHOEXJAh6SmhCqLqs9+tLDLpJPzB35RPMBVMJSF9+uUuCLgt85\nDp6pfvItY5ahcCKM5QsulI8ruoetCyg4u8URiKEuIHKmpL368UJp/E/d0m0hqVUy\nwPv3GIup8Yu4N5U637CzPnZifOdPkJoXAPKqqbd7OzmZo33tKyy/9KnuzPosZrIb\nFPPiL/SbAgMBAAECggEAUsxRqqqu3m19+4Ps+wKO2r4Du2UjAiYhN61iIauDC2Uk\nFT54orP2+5nPSqKst+8ZnpiglxT2L/0VOMjkal/Tu7c8Uuv1zEhq4vLHYtIlM27n\naQasA0ksNdVLcSYKx8i9WFMIw7jDiVkgVdsA2rqt9lXyEkauScYyMeDHJ1MLf9K+\nkWesmLUCqcAK3hFj0DmoXxA0yutezmpu0/GsP6EN7U2NP6JbGEJufUT5MEsFd4Iy\nv+/Ju5SCBe4C3dU/kmQRiGe4cOIfAfpAX+D7INg+c5XgcvC0LM+oscsF7Mbv+RO2\ngKHq+S7DC8XY6p2SU+tfNxJYSXV+j4uGZqBL/mTiIQKBgQDjiXknmN3j+jzp8kii\nSWE88mcLmY8HHAzrd/d18MrUWPCpyGncZJH8eupfbvX2SEubDXg/FjYtLSRFbGkj\n/kxEK3VD2XSi3g1I2aZRpWFcQ4kOwdXGKYRpysvZpfWVVdPlst6IKoaWiMsowVpc\njPmL7sMVJovyoaOLwIKCVn5uMQKBgQDD4mt1XJZB1wL0mWtd+Hmhm2JOODK2ptHl\nR0aEsoQeXnppYlR0k7MHm4dfYBTX8iTEpve2amMns733ht3lHQjXRiXap8BAAAhJ\neWXhENLH+RqtqeTNReCGi4Cis4kc5dI6ci02U1dbxR1jWU3lFRKgzih+ZrVi3wK9\n2zxZdQQgiwKBgEoKnCAkgGAFN/kw3C3jDv9UKe/+YiqAGDwvZ5k7Z+U9o+K70qbj\nGxY7sTSo2OTxlZi5A14UDkEZJWpp4rTRtWOheKFBi9DLzSrdmF+NH2d0mo7wyG2Z\naPs8AKWseYPE0BU45W0N2Yn2rZVvhcYEIdhFM7oYNwYXTy7U0CM1niKhAoGBAJ+g\nPW1wO8n/ixsEguga+hNl6mBPj5QksVskECVGzD9rQlnqlWRCkFwcs95XD+VCIBBY\n9/ghDhhmcuHpd52jYXb8ZRCgLxx4akhls04LRZlMOB9ZGWjmDqfVusfbglqYNg8G\nyC1DESdppB83wTBzUbDVD06iKCIgFb3dkHpdzzUtAoGBANaiy5HpZfiMLCNOMAME\nM76evgxpGoAtVnNyryDPOQ2YcSgMy0eNoP0wrH2isFE+zFNAS6QSnESyKdTClvSa\n/9TWg9GVzcKsuilm3wdk7TFjSi1c0tdkN2lUJnJU7f1WxSvucDaG8WnTuO6q0feR\noLuHL7Ev4boTOZGCnmTA9jEL\n-----END PRIVATE KEY-----\n\", \"client_email\": \"apitester@youtube-275008.iam.gserviceaccount.com\", \"client_id\": \"107777956579442973175\", \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\", \"token_uri\": \"https://oauth2.googleapis.com/token\", \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\", \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/apitester%40youtube-275008.iam.gserviceaccount.com\" }");
                InsertYoutubeCredential("{ \"type\": \"service_account\", \"project_id\": \"riaan-playground\", \"private_key_id\": \"b1fcb21fe8e14b82db914dc41e58c2731b14eb48\", \"private_key\": \"-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDFWT/1dN2bobMh\nWEBCEotA5LUkEzrs51592zIlW/L953HfEsUii13nY/iELvNhuasTfdiUD0skYj2r\ngjWBm3t298FbjJsPXQIbmC46SXr0e6LKO11dVeKNR6mjW2hXAJX7jCPW77s/+vVo\nqQm+FZJoDUw8b7KXqwwfmqdRgCmvhkBwm73TCZqXaGVtpAFyvQt+EvRW3+bBU7Oz\n0UNYXmhFDno41SDZDmnoQPbySbhq2L7ZJLmbXsSo8XMm2C6M0Ww4V8zOYNIqqF/t\n9ANTrtGrC84CORuoFRT+ZRP4bptlbm7l6Eh9ck8i3iCGszLmLHcvg7lmA/vL32n1\np2P1U7K/AgMBAAECggEAAieQyrG9b6MKzRzlCrOgIJO+pIevN4HpJq+bcwv3ca0y\nsZBHZbKm0k1lm6/ViWYZB4vyrt8h32JRMQupYOJdLTXUPxcgZEEMMlftfUZrGLs2\n0kVmpgu3fvpe0EFBuN1xRb9IbuLMJQ0s27kouZILuKYYhHpltUExTzEFjZ68FcX5\nCp/vYhSAChvyTNgW4KFGk2vkRhRcUL4H7LMpRZs6y6os7EAUsEFNs/Scmb2YYcSY\nINywvAonal9ksVwut4h31B3J97a5bWVHB6J1Ox3RmyQ7bd1Kfj2cn5+tmLCRgNRr\n++30fPru7NTRAOp2Nq1yfih04sfPGPEvp6r5AV+GvQKBgQDjV9uEXAsEq/hMUvF3\nc5TvMT7CZ9dy5Nvfvp/nWRRlxpi74gfpwwmitnpyqBdMo6p2jmguubNEJU4d7ukR\nXgKUWoPDEagHMJ2kkfm+sLjd3ISaEwbWO8Vl+mXJ/RltUFjwIL7y6j+ehKeBhaKs\npLUs7mFhW8t29Tbdf+5J+ypTMwKBgQDeOX9ITsjfw7pfTnAW419+n0++BP04QIad\n2UEcwFEzXs5EADb2681V3FRccupIOgpchdZE2dahx4II6w2teYUwCC8+sHz9vYB4\n12uOrD7cvk53s4uXWJhdLQkMqvMTdpvpn8tDtWUsI/DpKa+yCQ5laLAxUEc7Zr87\n1H4A/KeiRQKBgFZxNpeKQ+o/1UZ4FzY7LXsRd3KF92bJXE0j9zsJJPdZ3wM5e5bH\nwScsSTzFay6J4Kv5k+WIWIBlyuEY0Wf4HfT4wbbvRLuPIbHMi3CVU4WQbp8C8wuM\naI9Q+VknMn7dGSQc314P8aa9e8+E14jbi8QM09woAiI2NOld1mYs6Tf/AoGAZZQ4\nt0+0CHzKg2gyWnQCobmm1MApE36DSkIVvlO2ilnKxVCnz0TILngxY7TvnKZTOCnB\nYOWGc5rjuLnhHavPkr4bAnfgGoh14kbuFmBrt7GR4quComLP+r9sRIsb44izCh6N\n5lxTa9Ld+2aftiznlLk+2LvOY7u6rFmcY00aP1UCgYBDCykiY2yOsLuHVCBLD54T\nSFT3Tt4m5kCTA/smvVfsCLAeoiZSRUq5a0nhFjG7nEkFRgpjcO+GdGGhRHKTCu1z\nqGeLzpomQa7gCc4MeNb/0GHEogAoH7g/vV45xhZfw0hl0vIiDkpP4jZR6gBDzZxl\nD6wrVndChfQLZtMsUR6zsg==\n-----END PRIVATE KEY-----\n\", \"client_email\": \"apitester@riaan-playground.iam.gserviceaccount.com\", \"client_id\": \"109409810288560828741\", \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\", \"token_uri\": \"https://oauth2.googleapis.com/token\", \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\", \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/apitester%40riaan-playground.iam.gserviceaccount.com\" }");
            }

            if (!_unitOfWork.Query<YoutubeVideoFaq>().Any())
            {
                InsertFaq();
            }

            _unitOfWork.Commit();
        }

        private void InsertYoutubeCredential(string json)
        {
            var yc = JsonConvert.DeserializeObject<YoutubeCredential>(json);
            yc.LockedUntil = DateTime.UtcNow;
            _unitOfWork.Insert(yc);
        }

        private void InsertFaq()
        {
            // 2D Character Movement
            var video = _youtubeRepository.GetVideoByRemoteId("n4N9VEA2GFo").Result;
            if (video != null)
            {
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why is my character moonwalking", Answer = "This tutorial does not cover animations. 2D sprites can often look strange when moved without animation" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why is my character sliding", Answer = "You are not stopping the motion of your character and letting physics stop him. You have to either stop the character when needed, or push the rigidbody force in the opposite direction" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "My character is moving in the wrong direction", Answer = "You most likely have the sign of your X axis wrong way around. Try making making it positive or negative depending on your current value" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "My character jumps, but falls back to the ground slowly", Answer = "Try adjusting the physics setting by lowering the gravity" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why is my character not moving", Answer = "Make sure the movement speed in Unity is set to more than 0. The default value you give to the property in C# gets overridden by what you set in Unity" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "How do I make my character look in the direction he's walking", Answer = "See the bonus section of this tutorial" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "My code doesn't seem to work", Answer = "Join us on Discord (<a href='https://discord.gg/EVJ2wpt' target='_blank'>https://discord.gg/EVJ2wpt</a>) and send us a screenshot or more details" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Am I allowed to use these assets in my game", Answer = "We only use free assets for these tutorials unless stated otherwise. You can use these assets in your projects, but you should make sure to look at the licence information in the links we provide. For some you may need to give attribution when used. Each creator can have their own requests for how you use their work" });
                //TODO: Setup proper link
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "How do I install Visual Studio", Answer = "You can (and should probably) install it via Unity Hub. See our <a href='https://discord.gg/EVJ2wpt' target='_blank'>Visual Studio Setup video</a>" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "My Visual Studio is not hooked up to Unity", Answer = "After installing Visual Studio, make sure it is set as your external script editor. You can do so in Unity via Edit -> Preferences -> External Tools -> External Script Editor" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why do I get a 'namespace cannot directly contain members such as fields or methods' error", Answer = "C# classes can not start with numeric characters" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Should my class name and file name match", Answer = "Yes" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why is my collider not working", Answer = "If you are working in a 2D space, you should be using a 2D collider" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Is C# case sensitive", Answer = "Yes" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why does my script not load in Unity", Answer = "You either have errors or renamed the file" });
            }

            // 2D Projectiles
            video = _youtubeRepository.GetVideoByRemoteId("8TqY6p-PRcs").Result;
            if (video != null)
            {
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why is my projectile falling", Answer = "Freeze y axis on rigidbody" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why is my projectile on in the correct location", Answer = "You forgot to reset the transform positions in the prefab" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "My projectile is going through objects and enemies", Answer = "You either missing a rigidbody and/or collider, or you are not using 2D for both the colliders and rigidbodies" });
            }

            // 2D Enemy Hitpoints
            video = _youtubeRepository.GetVideoByRemoteId("7DPElwMtc9Y").Result;
            if (video != null)
            {
                //_unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "xxx", Answer = "xxx" });
            }

            // 2D Enemy Health Bars
            video = _youtubeRepository.GetVideoByRemoteId("v1UGTTeQzbo").Result;
            if (video != null)
            {
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why is my health bar overlapping with my enemy's head", Answer = "Your offset is too low for your character. Just adjust the offset property accordingly" });
            }

            // 2D Launchable Projectiles
            video = _youtubeRepository.GetVideoByRemoteId("l8K3KmOJUsI").Result;
            if (video != null)
            {
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why did my launchable projectile replace my primary projectile", Answer = "You should use Fire1 and Fire2. If you make both the same Input, one will replace the other" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "My bomb disposes immediately", Answer = "Your offset is too close to your character, causing the bomb to spawn and collide with you inseat of traveling to its destination" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why is my launchable projectile going the wrong way", Answer = "You most likely have the sign of your X axis wrong way around. Try making making it positive or negative depending on your current value" });
            }

            // Dark mode
            video = _youtubeRepository.GetVideoByRemoteId("76shGi492r4").Result;
            if (video != null)
            {
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Is it really legal", Answer = "Yes. Unity's legal team stated that its fine.<br>See this forum topic : <a href='https://forum.unity.com/threads/editor-skinning-thread.711059/page-2#post-5620048' target='_blank'>https://forum.unity.com/threads/editor-skinning-thread.711059/page-2#post-5620048</a>" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "How is this any more legal than hex editing", Answer = "Hex editing changes the binary files, which is against the EULA and also against the law in most countries. This is using the tools supplied by unity and their legal team stated that its fine.<br>See this forum topic : <a href='https://forum.unity.com/threads/editor-skinning-thread.711059/page-2#post-5620048' target='_blank'>https://forum.unity.com/threads/editor-skinning-thread.711059/page-2#post-5620048</a>" });
                //TODO: Setup proper link
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Do you own this project", Answer = "No, we merely found it amazing and wanted to share it. The owner can be found at ~github link~" });
                //TODO: Setup proper link
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Can I see the sourcecode", Answer = "Sure, head over to github (~github link~)" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Can I sell the code as my own", Answer = "No you can not" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Do I have to pay to use darkmode", Answer = "No, it is completely free to use" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "I get an error when trying to add the URL", Answer = "You need to install GIT first" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Does the tool work on all versions of unity", Answer = "No. Currently it is only available for versions of 2019 from 2019.3 and up" });
                //TODO: Setup proper link
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Is this compatible with Unity 2020", Answer = "No, not yet. The creator is working on it though. See the progress at ~github link~" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Why did all my icons become pixelated", Answer = "You are using Unity 2020. This project is not compatible with newer versions of Unity yet" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Which options should I select when installing GIT", Answer = "The defaults are fine" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "I installed GIT, but it still does not appear in Unity", Answer = "You should restart Unity and Unity Hub after installing GIT" });
            }

            // Visual studio
            video = _youtubeRepository.GetVideoByRemoteId("Q39VWaqlCKs").Result;
            if (video != null)
            {
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "What is the difference between Visual Studio and Visual Studio Code", Answer = "Visual Studio code is more lightweight and configurable for a range of stuff, Visual Studio is way more powerful and unity can install it and is thus immediately usable by a newer user, and thus more 'beginner' friendly" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Do I have to use Visual Studio", Answer = "Not at all. You can use whatever has your preference" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "How much do I need to pay to use Visual Studio", Answer = "Visual Studio Community is free to use" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "Which version should I install", Answer = "We recommend always using the latest stable release" });
                _unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "My Visual Studio is not hooked up to Unity", Answer = "After installing Visual Studio, make sure it is set as your external script editor. You can do so in Unity via Edit -> Preferences -> External Tools -> External Script Editor" });
            }

            // Neon runner
            video = _youtubeRepository.GetVideoByRemoteId("zYiiiYZcjOw").Result;
            if (video != null)
            {
                //_unitOfWork.Insert(new YoutubeVideoFaq { Video = video, Question = "xxx", Answer = "xxx" });
            }
        }
    }
}