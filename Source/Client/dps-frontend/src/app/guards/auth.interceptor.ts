import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from "@angular/common/http";

import { AuthenticationService } from "src/api/authentication.service";
import { environment } from "src/environments/environment";
import { Observable, Subject } from "rxjs";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private readonly authenticationService: AuthenticationService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.isApiCall(req)) {
            const currentToken = this.authenticationService.currentToken;
            if (this.authenticationService.isValid(currentToken)) {
                return this.mutateRequest(req, next);
            } else {
                return this.propulateSubjectByLoadingToken(req, next);
            }
        }

        return next.handle(req);
    }

    isApiCall(req: HttpRequest<any>): boolean {
        return req.url.indexOf("~/api/") === 0;
    }

    propulateSubjectByLoadingToken(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Try loading it from the authentication service, then, whatever happens, populate the request
        const subject = new Subject<HttpEvent<any>>();
        this.authenticationService.tryLoadFromCookie().finally(() => {
            // Mutate and handle the request then jam it into the subject
            this.mutateRequest(req, next)
                .toPromise()
                .then((result) => {
                    subject.next(result);
                    subject.complete();
                });
        });
        return subject.asObservable();
    }

    mutateRequest(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Add the auth tokens and adapt the url
        const currentToken = this.authenticationService.currentToken;
        if (this.authenticationService.isValid(currentToken)) {
            req = req.clone({
                setHeaders: { "x-auth-token": currentToken.authToken },
            });
        }
        req = req.clone({ url: req.url.replace("~", environment.gatewayUrl) });
        return next.handle(req);
    }
}
