import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { Component } from '@angular/core';

@Component({
    selector: "bottom-sheet-content",
    template: "<div [innerHtml]='Content' style='padding:30px 0'></div>",
})
export class BottomContentSheet {
    Content = "";

    constructor(private _bottomSheetRef: MatBottomSheetRef<BottomContentSheet>) {
        this.Content = _bottomSheetRef.containerInstance.bottomSheetConfig.data.content;
        console.log("Con:", _bottomSheetRef);
        console.log("Con:", this.Content);
    }

    openLink(event: MouseEvent): void {
        this._bottomSheetRef.dismiss();
        event.preventDefault();
    }
}
