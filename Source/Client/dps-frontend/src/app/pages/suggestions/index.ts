import { Component, OnInit } from "@angular/core";
import { DataService, SuggestionDto } from "src/api/data.service";

@Component({
    selector: "app-suggestions-index",
    templateUrl: "index.html",
    styles: [],
})
export class SuggestionIndexComponent implements OnInit {
    Suggestions: SuggestionDto[] = [];

    constructor(private readonly DataService: DataService) {}

    ngOnInit(): void {
        this.DataService.getSuggestions().then((results) => {
            this.Suggestions = results.sort((a, b) => b.votes - a.votes);
        });
    }
}
