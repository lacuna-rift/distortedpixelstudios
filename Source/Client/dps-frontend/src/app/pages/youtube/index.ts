import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { YoutubePlaylistDto, YoutubeVideoDto } from "src/api/data.service";
import { BottomContentSheet } from 'src/app/components/bottom-sheet-content';

@Component({
    selector: "app-youtube-index",
    templateUrl: "index.html",
    styles: [],
})
export class YoutubeIndexComponent {
    Playlists: YoutubePlaylistDto[];
    Videos: YoutubeVideoDto[];

    constructor(private readonly route: ActivatedRoute, private _bottomSheet: MatBottomSheet) {
        this.Playlists = this.route.snapshot.data.playlists;
        this.Videos = this.route.snapshot.data.videos;
    }

    openBottomSheet(content: string): void {
        this._bottomSheet.open(BottomContentSheet, { data: { content: content } });
    }
}
