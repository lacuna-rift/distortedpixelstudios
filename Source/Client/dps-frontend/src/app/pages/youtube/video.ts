import { Component, OnInit } from "@angular/core";
import {
    DataService,
    YoutubePlaylistDto,
    YoutubeVideoDto,
    YoutubeVideoDownloadablesDto,
    YoutubeVideoCommentDto,
    YoutubeVideoDownloadableTypes,
    YoutubeVideoFaqDto,
} from "src/api/data.service";
import { ActivatedRoute, Router } from "@angular/router";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { environment } from "src/environments/environment";
import { MatTabChangeEvent } from "@angular/material/tabs";
import { Location, LocationStrategy, PathLocationStrategy } from "@angular/common";
import { BottomContentSheet } from "src/app/components/bottom-sheet-content";
import { MatBottomSheet } from '@angular/material/bottom-sheet';

@Component({
    selector: "app-youtube-video",
    templateUrl: "video.html",
    styles: [],
    providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }],
})
export class YoutubeVideoComponent {
    YoutubeVideoDownloadableTypes = YoutubeVideoDownloadableTypes;

    tabStuff = ["comments", "faq", "resources"];

    Playlists: YoutubePlaylistDto[];
    Videos: YoutubeVideoDto[];

    SelectedVideo: YoutubeVideoDto;
    SelectedTabIndex = 0;
    ViewUrl: SafeResourceUrl;
    EmbedUrl: SafeResourceUrl;
    Downloadables: YoutubeVideoDownloadablesDto[] = [];
    Comments: YoutubeVideoCommentDto[] = [];
    FAQs: YoutubeVideoFaqDto[] = [];

    HideThumbnail = false;

    constructor(
        private readonly DataService: DataService,
        private readonly location: Location,
        private readonly route: ActivatedRoute,
        private readonly sanitizer: DomSanitizer,
        private _bottomSheet: MatBottomSheet
    ) {
        this.Playlists = this.route.snapshot.data.playlists;
        this.Videos = this.route.snapshot.data.videos;
        this.route.params.subscribe((p) => {
            this.SelectedTabIndex = Math.max(
                0,
                this.tabStuff.findIndex((o) => o == p.tab)
            );
            if (this.SelectedVideo == null || this.SelectedVideo.remoteId != p.videoRemoteId) {
                this.SelectedVideo = this.Videos.find((o) => o.remoteId == p.videoRemoteId);
                this.ViewUrl = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/watch/" + this.SelectedVideo.remoteId);
                this.EmbedUrl = this.sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/" + this.SelectedVideo.remoteId);
                this.HideThumbnail = false;
                this.Downloadables = [];
                this.Comments = [];
                this.FAQs = [];
                this.DataService.getDownloadables(this.SelectedVideo).then((results) => {
                    this.Downloadables = results;
                    this.scrollToTab(this.route.snapshot.params.tab);
                });
                this.DataService.getComments(this.SelectedVideo).then((results) => {
                    this.Comments = results;
                    this.scrollToTab(this.route.snapshot.params.tab);
                });
                this.DataService.getFaqs(this.SelectedVideo).then((results) => {
                    this.FAQs = results;
                    this.scrollToTab(this.route.snapshot.params.tab);
                });
            }
        });
    }
    ngAfterViewInit(): void {
        this.scrollToTab(this.route.snapshot.params.tab);
    }

    scrollToTab(tab: string): void {
        if (tab && tab.length > 0) {
            document.getElementById(tab).scrollIntoView();
        }
    }

    getDownloadableHandle(downloadable: YoutubeVideoDownloadablesDto): SafeResourceUrl {
        return this.sanitizer.bypassSecurityTrustResourceUrl(
            environment.gatewayUrl + `/api/youtube/video/${this.SelectedVideo.remoteId}/downloadables/${downloadable.handle}`
        );
    }

    getVidsByPlaylist(playlist: YoutubePlaylistDto): YoutubeVideoDto[] {
        return this.Videos.filter((v) => v.playlistId == playlist.id);
    }
    previousVideoInSeries(): YoutubeVideoDto {
        var playlist = this.Videos.filter((v) => v.playlistId == this.SelectedVideo.playlistId);
        var results = playlist.filter((v) => v.publishDate < this.SelectedVideo.publishDate).sort((a, b) => b.publishDate - a.publishDate);
        if (results.length > 0) return results[0];
        return null;
    }

    nextVideoInSeries(): YoutubeVideoDto {
        var playlist = this.Videos.filter((v) => v.playlistId == this.SelectedVideo.playlistId);
        var results = playlist.filter((v) => v.publishDate > this.SelectedVideo.publishDate).sort((a, b) => a.publishDate - b.publishDate); // inverse order
        if (results.length > 0) return results[0];
        return null;
    }

    selectedIndexChanged(tabChangeEvent: MatTabChangeEvent): void {
        var name = this.tabStuff[(tabChangeEvent as any) as number];
        this.location.replaceState(`/youtube/${this.SelectedVideo.remoteId}/show/${name}`);
    }

    openBottomSheet(content: string): void {
        this._bottomSheet.open(BottomContentSheet, { data: { content: content } });
    }
}
