import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MatBottomSheet } from "@angular/material/bottom-sheet";
import { YoutubePlaylistDto, YoutubeVideoDto } from "src/api/data.service";
import { BottomContentSheet } from 'src/app/components/bottom-sheet-content';

@Component({
    selector: "app-youtube-playlist",
    templateUrl: "playlist.html",
    styles: [],
})
export class YoutubePlaylistComponent {
    Playlists: YoutubePlaylistDto[];
    Videos: YoutubeVideoDto[];

    SelectedPlaylist: YoutubePlaylistDto;
    SelectedPlaylistVideos: YoutubeVideoDto[];

    constructor(private readonly route: ActivatedRoute, private _bottomSheet: MatBottomSheet) {
        this.Playlists = this.route.snapshot.data.playlists;
        this.Videos = this.route.snapshot.data.videos;
        this.route.params.subscribe((p) => {
            this.SelectedPlaylist = this.Playlists.find((o) => o.id == p.playlistId);
            this.SelectedPlaylistVideos = this.Videos.filter((v) => v.playlistId == this.SelectedPlaylist.id);
        });
    }

    openBottomSheet(content: string): void {
        this._bottomSheet.open(BottomContentSheet, { data: { content: content } });
    }
}