import { Component, OnInit } from "@angular/core";
import { DataService, YoutubePlaylistDto, YoutubeVideoDto, VideosResolver, PlaylistResolver } from "src/api/data.service";

@Component({
    selector: "app-root",
    templateUrl: "app.html",
    styles: [],
})
export class AppComponent {
    Playlists: YoutubePlaylistDto[];
    Videos: YoutubeVideoDto[];

    constructor(playlistResolver: PlaylistResolver, videosResolver: VideosResolver) {
        videosResolver.resolve().then((results) => {
            this.Videos = results;
        });
        playlistResolver.resolve().then((results) => {
            this.Playlists = results;
        });
    }
    getVidsByPlaylist(playlist: YoutubePlaylistDto): YoutubeVideoDto[] {
        return this.Videos.filter((v) => v.playlistId == playlist.id);
    }
}
