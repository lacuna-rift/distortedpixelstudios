import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { IndexComponent } from "./pages";

import { YoutubeVideoComponent } from "./pages/youtube/video";
import { YoutubePlaylistComponent } from "./pages/youtube/playlist";
import { YoutubeIndexComponent } from "./pages/youtube";
import { SuggestionIndexComponent } from "./pages/suggestions";
import { VideosResolver, PlaylistResolver } from "src/api/data.service";
import { UdemyIndexComponent } from "./pages/udemy";
import { AssetsIndexComponent } from "./pages/assets";

const routes: Routes = [
    {
        path: "udemy",
        component: UdemyIndexComponent,
    },
    {
        path: "youtube",
        component: YoutubeIndexComponent,
        resolve: { videos: VideosResolver, playlists: PlaylistResolver },
    },
    {
        path: "youtube/:videoRemoteId",
        component: YoutubeVideoComponent,
        resolve: { videos: VideosResolver, playlists: PlaylistResolver },
    },
    {
        path: "youtube/:videoRemoteId/show/:tab",
        component: YoutubeVideoComponent,
        resolve: { videos: VideosResolver, playlists: PlaylistResolver },
    },
    {
        path: "youtube/playlist/:playlistId",
        component: YoutubePlaylistComponent,
        resolve: { videos: VideosResolver, playlists: PlaylistResolver },
    },
    {
        path: "suggestions",
        component: SuggestionIndexComponent,
    },
    {
        path: "assets",
        component: AssetsIndexComponent,
    },
    {
        path: "**",
        redirectTo: "youtube",
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
