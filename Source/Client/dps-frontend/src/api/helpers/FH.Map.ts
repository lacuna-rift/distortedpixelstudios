export class FHMap {
    static DateFields<T>(propertyFunction: (o: T) => Date): (o: T[]) => T[] {
        return results => this.DateFieldsInline(results, propertyFunction);
    }

    static DateFieldsInline<T>(results: T[], propertyFunction: (o: T) => Date): T[] {
        const property = FHMap.getPropertyName(propertyFunction);
        results.forEach(obj => {
            FHMap.InternalDateField(obj, property);
        });
        return results;
    }

    static DateFieldInline<T>(result: T, propertyFunction: (o: T) => Date): T {
        const property = FHMap.getPropertyName(propertyFunction);
        FHMap.InternalDateField(result, property);
        return result;
    }

    private static InternalDateField<T>(obj: T, property: string): void {
        const value = obj[property];
        if (typeof value === "string" && value != null && value.length > 0) {
            obj[property] = new Date(value);
        }
    }

    private static getPropertyName(propertyFunction: (o: any) => any): string {
        // REGEX
        // Start from the last .
        // Allow a-z
        // Allow A-Z
        // Allow 0-9
        // Allow _
        // Take the first group
        try {
            return /\.([a-zA-Z0-9\_]+)$/.exec(propertyFunction.toString())[1];
        } catch (e) {
            console.error("Could not get the property name from this function : ", propertyFunction);
            console.log(e);
            return null;
        }
    }
}
