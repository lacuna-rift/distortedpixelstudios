import { Resolve } from "@angular/router";

export abstract class FHCachedResolver<T> implements Resolve<T> {
    private promise: Promise<T>;

    resolve(): Promise<T> {
        this.promise =
            this.promise ??
            new Promise<T>((resolve) => {
                if (this.getCacheData() == null) {
                    this.getFreshData().then((result) => {
                        this.setCacheData(result);
                        resolve(this.createClone(result));
                        this.promise = null;
                    });
                } else {
                    resolve(this.createClone(this.getCacheData()));
                    this.promise = null;
                }
            });
        return this.promise;
    }
    protected abstract setCacheData(data: T): void;
    protected abstract getCacheData(): T;
    protected abstract getFreshData(): Promise<T>;
    private createClone(input: T): T {
        return JSON.parse(JSON.stringify(input));
    }
}
