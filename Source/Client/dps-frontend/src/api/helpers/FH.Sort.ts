export class FHSort {
    // sorts by many fields
    // the same as chaining .OrderBy(...).ThenBy(...).ThenBy(...).ThenBy(...).ThenBy(...) in c#
    static sort<T>(...getters: ((a: T) => number | boolean | string | Date)[]): (a: T, b: T) => number {
        return (a, b) => {
            for (let i = 0; i < getters.length; i++) {
                const getter = getters[i];
                const aValue = getter(a);
                const bValue = getter(b);

                if (aValue > bValue) {
                    return 1;
                } else if (aValue < bValue) {
                    return -1;
                }
            }
            return 0;
        };
    }
}
