// Packages
import { Injectable } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { HttpClient } from "@angular/common/http";

// Service Specification
import { FHMap } from "./helpers/FH.Map";

@Injectable({ providedIn: "root" })
export class AuthenticationService {
    public currentToken: AuthenticationTokenDto | null = null;

    private readonly cookieName = "auth-token";

    private refreshTimerPadding = 5000;
    private refreshTimer: number;

    constructor(private readonly cookieService: CookieService, private readonly httpClient: HttpClient) {}

    refreshToken(): Promise<AuthenticationTokenDto> {
        return new Promise((resolve, reject) => {
            if (this.currentToken == null) {
                reject();
            } else {
                this.httpClient
                    .post<AuthenticationTokenDto>(`/api/auth/refresh`, this.currentToken)
                    .toPromise()
                    .then(result => FHMap.DateFieldInline(result, o => o.refreshAvailableFrom))
                    .then(result => FHMap.DateFieldInline(result, o => o.refreshAvailableUntil))
                    .then(result => {
                        if (!this.isValid(result)) {
                            this.setToken(null);
                            reject("Invalid token received from the server");
                        } else {
                            this.setToken(result);
                            resolve(result);
                        }
                    })
                    .catch(error => {
                        this.setToken(null);
                        reject(error);
                    });
            }
        });
    }

    authenticate(email: string, password: string): Promise<AuthenticationTokenDto> {
        if (this.refreshTimer != null) {
            clearTimeout(this.refreshTimer);
        }

        return new Promise((resolve, reject) => {
            const dto = new SignInDto();
            dto.email = email;
            dto.password = password;
            this.httpClient
                .post<AuthenticationTokenDto>(`/api/auth/authenticate`, dto)
                .toPromise()
                .then(result => FHMap.DateFieldInline(result, o => o.refreshAvailableFrom))
                .then(result => FHMap.DateFieldInline(result, o => o.refreshAvailableUntil))
                .then(result => {
                    if (!this.isValid(result)) {
                        this.setToken(null);
                        reject("Invalid token received from the server");
                    } else {
                        this.setToken(result);
                        resolve(result);
                    }
                })
                .catch(error => {
                    this.setToken(null);
                    reject(error);
                });
        });
    }

    private setToken(newToken: AuthenticationTokenDto): void {
        if (this.refreshTimer != null) {
            clearTimeout(this.refreshTimer);
        }
        this.currentToken = newToken;

        if (newToken == null) {
            return;
        }
        const timeNow = this.getNowUtc();
        const minSleepTime = newToken.refreshAvailableFrom.getTime() - timeNow.getTime() + this.refreshTimerPadding;
        const maxSleepTime = newToken.refreshAvailableUntil.getTime() - timeNow.getTime() - this.refreshTimerPadding;
        const sleepTime = minSleepTime + Math.random() * (maxSleepTime - minSleepTime);
        this.refreshTimer = window.setTimeout(() => {
            if (this.currentToken != null) {
                this.refreshToken();
            }
        }, sleepTime);

        this.cookieService.set(this.cookieName, JSON.stringify(newToken), 7, "/");
    }

    getIsLoggedIn(): Promise<boolean> {
        return new Promise<boolean>((result, reject) => {
            this.tryLoadFromCookie()
                .then(() => {
                    result(true);
                })
                .catch(() => {
                    result(false);
                });
        });
    }

    isValid(authenticationTokenDto: AuthenticationTokenDto): boolean {
        const now = this.getNowUtc();
        if (authenticationTokenDto == null) {
            return false;
        }
        return now < authenticationTokenDto.refreshAvailableFrom;
    }

    canRefresh(authenticationTokenDto: AuthenticationTokenDto): boolean {
        const now = this.getNowUtc();
        if (authenticationTokenDto == null) {
            return false;
        }
        return authenticationTokenDto.refreshAvailableFrom < now && now < authenticationTokenDto.refreshAvailableUntil;
    }

    private getNowUtc(): Date {
        const now = new Date();
        return new Date(now.getTime() + now.getTimezoneOffset() * 60000);
    }

    clear(): void {
        this.currentToken = null;
        this.cookieService.delete(this.cookieName, "/");
    }

    tryLoadFromCookie(): Promise<void> {
        return new Promise<void>((result, reject) => {
            const token = this.cookieService.get(this.cookieName);
            if (token == null) {
                reject(); // #FAIL
            }
            const storgeToken = JSON.parse(token) as AuthenticationTokenDto;

            FHMap.DateFieldInline(storgeToken, o => o.refreshAvailableFrom);
            FHMap.DateFieldInline(storgeToken, o => o.refreshAvailableUntil);

            if (this.isValid(storgeToken)) {
                this.currentToken = storgeToken;
                result(); // #SUCCESS
            } else {
                if (this.canRefresh(storgeToken)) {
                    this.refreshToken()
                        .then(() => {
                            result(); // #SUCCESS
                        })
                        .catch(() => {
                            reject(); // #FAIL
                        });
                } else {
                    reject(); // #FAIL
                }
            }
        });
    }
}






export class AuthenticationTokenDto {
    authToken: string;
    refreshAvailableFrom: Date; // Date Mapped
    refreshAvailableUntil: Date; // Date Mapped
}

export class SignInDto {
    email: string;
    password: string;
}