// Packages
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { FHCachedResolver } from "./helpers/FHCachedResolver";

@Injectable({ providedIn: "root" })
export class DataService {
    constructor(private readonly httpClient: HttpClient) {}

    getPlaylists(): Promise<YoutubePlaylistDto[]> {
        return this.httpClient.get<YoutubePlaylistDto[]>(`~/api/youtube/playlists`).toPromise();
    }
    getVideos(): Promise<YoutubeVideoDto[]> {
        return this.httpClient.get<YoutubeVideoDto[]>(`~/api/youtube/videos`).toPromise();
    }
    getDownloadables(video: YoutubeVideoDto): Promise<YoutubeVideoDownloadablesDto[]> {
        return this.httpClient.get<YoutubeVideoDownloadablesDto[]>(`~/api/youtube/video/${video.remoteId}/downloadables`).toPromise();
    }
    getComments(video: YoutubeVideoDto): Promise<YoutubeVideoCommentDto[]> {
        return this.httpClient.get<YoutubeVideoCommentDto[]>(`~/api/youtube/video/${video.remoteId}/comments`).toPromise();
    }
    getFaqs(video: YoutubeVideoDto): Promise<YoutubeVideoFaqDto[]> {
        return this.httpClient.get<YoutubeVideoFaqDto[]>(`~/api/youtube/video/${video.remoteId}/faqs`).toPromise();
    }
    getSuggestions(): Promise<SuggestionDto[]> {
        return this.httpClient.get<SuggestionDto[]>(`~/api/youtube/suggestions`).toPromise();
    }
}

// ======================================>
// ======================================>
// ====> Resolvers
// ======================================>
// ======================================>

@Injectable({ providedIn: "root" })
export class PlaylistResolver extends FHCachedResolver<YoutubePlaylistDto[]> {
    private static cachedData: YoutubePlaylistDto[] = null;

    constructor(private readonly service: DataService) {
        super();
    }

    protected getFreshData(): Promise<YoutubePlaylistDto[]> {
        return this.service.getPlaylists();
    }
    protected setCacheData(data: YoutubePlaylistDto[]): void {
        PlaylistResolver.cachedData = data;
    }
    protected getCacheData(): YoutubePlaylistDto[] {
        return PlaylistResolver.cachedData?.sort((a, b) => a.displayOrder - b.displayOrder);
    }
}

@Injectable({ providedIn: "root" })
export class VideosResolver extends FHCachedResolver<YoutubeVideoDto[]> {
    private static cachedData: YoutubeVideoDto[] = null;

    constructor(private readonly service: DataService) {
        super();
    }

    protected getFreshData(): Promise<YoutubeVideoDto[]> {
        return this.service.getVideos();
    }
    protected setCacheData(data: YoutubeVideoDto[]): void {
        data.forEach((v) => {
            v.name = v.name.replace(" in Unity / 2020", "");
        });
        VideosResolver.cachedData = data;
    }
    protected getCacheData(): YoutubeVideoDto[] {
        return VideosResolver.cachedData?.sort((a, b) => a.displayOrder - b.displayOrder);
    }
}

// ======================================>
// ======================================>
// ====> MODELS
// ======================================>
// ======================================>

export enum YoutubeVideoDownloadableTypes {
    UnityPackage = 0,
}

export class SuggestionDto {
    id: number;
    name: string;
    votes: number;
}

export class YoutubeVideoDto {
    id: number;
    remoteId: string;
    name: string;
    description: string;
    thumbnail: string;
    likes: number;
    comments: number;
    views: number;
    downloadables: number;
    playlistId: number;
    publishDate: number;
    displayOrder: number;
}

export class YoutubePlaylistDto {
    id: number;
    name: string;
    url: string;
    displayOrder: number;
}

export class YoutubeVideoDownloadablesDto {
    name: string;
    description: string;
    type: YoutubeVideoDownloadableTypes;
    handle: string;
    displayOrder: number;
}

export class YoutubeVideoFaqDto {
    question: string;
    answer: string;
    displayOrder: number;
}

export class YoutubeVideoCommentDto {
    author: string;
    thumbnail: string;
    text: string;
    timestamp: number;
    likes: number;
    replies: YoutubeVideoCommentDto[];
    displayOrder: number;

    toggleReplies?: boolean;
}
