ng build --prod

copy .\Dockerfile .\dist\dps-frontend\Dockerfile
copy .\nginx-spa.conf .\dist\dps-frontend\nginx-spa.conf
copy .\nginx-compression.conf .\dist\dps-frontend\nginx-compression.conf

gcloud compute ssh dps-tips --command="sudo rm -rf /mnt/stateful_partition/lacuna-data/website/*"
gcloud compute scp --recurse  ./dist/dps-frontend/ dps-tips:/mnt/stateful_partition/lacuna-data/website
gcloud compute ssh dps-tips --command="sh /mnt/stateful_partition/lacuna-data/deploy.sh"